<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

use Bitrix\Main\Localization\Loc;
?>
<? foreach ($arResult["PAST_EVENTS"] as $arEvent) { ?>
        <div class="event-card disabled">
            <h3 class="event-card__title"><?= $arEvent["NAME"] ?></h3>
            <div class="event-card__date"><?= $arEvent["DATE_ACTION"] ?></div>
            <div class="event-card__btns">
                <a href="<?= $arEvent["DETAIL_PAGE_URL"] ?>" class="btn btn--default"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_EDIT_EVENT"); ?></a>
                <a href="#" class="btn btn--default disabled"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_CLOSE_EVENT") ?></a>
                <a href="<?=$arParams["EVENTS_INVITE_URL"]."?EVENT_ID=".$arEvent["ID"]?>" class="btn btn--default reset-filters"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_REGISTERED", ["#COUNT#" => $arEvent["CNT_ALL"]]); ?></a>
            </div>
            <div class="event-card__statistics">
                <?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_REFUSED", ["#COUNT#" => $arEvent["REFUSED"]]); ?><br>
    <?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_INVITED", ["#COUNT#" => $arEvent["INVITED"]]); ?><br>
            </div>
        </div>
<? } ?>


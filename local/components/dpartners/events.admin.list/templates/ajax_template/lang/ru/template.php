<?php
$MESS["CP_DP_EVENTS_ADMIN_LIST_FUTURE_TITLE"] = "Будущие события";
$MESS["CP_DP_EVENTS_ADMIN_LIST_PAST_TITLE"] = "Прошедшие события";
$MESS["CP_DP_EVENTS_ADMIN_LIST_ADD_EVENT"] = "Добавить событие";
$MESS["CP_DP_EVENTS_ADMIN_LIST_EDIT_EVENT"] = "Редактировать событие";
$MESS["CP_DP_EVENTS_ADMIN_LIST_REGISTERED"] = "Зарегистрировано #COUNT# человек";
$MESS["CP_DP_EVENTS_ADMIN_LIST_REFUSED"] = "Отклонено заявок: #COUNT#";
$MESS["CP_DP_EVENTS_ADMIN_LIST_INVITED"] = "Приглашены: #COUNT#";
$MESS["CP_DP_EVENTS_ADMIN_LIST_CLOSE_EVENT"] = "Закрыть регистрацию";
$MESS["CP_DP_EVENTS_ADMIN_MODAL_TITLE"] = "Вы уверены, что хотите завершить регистрацию на событие?";
$MESS["CP_DP_EVENTS_ADMIN_MODAL_OK"] = "Завершить";
$MESS["CP_DP_EVENTS_ADMIN_MODAL_CANCEL"] = "Отмена";


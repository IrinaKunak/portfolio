<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

use Bitrix\Main\Localization\Loc;
?>
<div class="events__top">
    <h2 class="page-title"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_FUTURE_TITLE"); ?></h2>
    <a href="/events-admin/ADD_ELEMENT/" class="btn btn--default"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_ADD_EVENT"); ?></a>
</div>
<div class="events__wrap">
    <? foreach ($arResult["FUTURE_EVENTS"] as $arEvent) { ?>
        <div class="event-card" data-id="<?= $arEvent["ID"] ?>">
            <h3 class="event-card__title"><?= $arEvent["NAME"] ?></h3>
            <div class="event-card__date"><?= $arEvent["DATE_ACTION"] ?></div>
            <div class="event-card__btns">
                <a href="<?= $arEvent["DETAIL_PAGE_URL"] ?>" class="btn btn--default"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_EDIT_EVENT"); ?></a>
                <? if ($arEvent["REG_CLOSE"]===false) { ?>
                <a href="#eventClosePopup" data-toggle="modal" data-target="#eventClosePopup" class="btn btn--default js-event-close"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_CLOSE_EVENT") ?></a>
                <? }else{ ?>
                <a href="#" class="btn btn--default disabled"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_CLOSED") ?></a>
                <?}?>
                <a href="<?=$arParams["EVENTS_INVITE_URL"]."?EVENT_ID=".$arEvent["ID"]?>" class="btn btn--default reset-filters"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_REGISTERED", ["#COUNT#" => $arEvent["CNT_ALL"]]); ?></a>
            </div>
            <div class="event-card__statistics">
                <?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_REFUSED", ["#COUNT#" => $arEvent["REFUSED"]]); ?><br>
                <?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_INVITED", ["#COUNT#" => $arEvent["INVITED"]]); ?><br>
            </div>
        </div>
    <? } ?>
</div>
<!--прошедшие события!-->
<h2 class="page-title"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_PAST_TITLE"); ?></h2>
<div class="events__wrap" id="past_events_wrap">
    <? foreach ($arResult["PAST_EVENTS"] as $arEvent) { ?>
        <div class="event-card disabled">
            <h3 class="event-card__title"><?= $arEvent["NAME"] ?></h3>
            <div class="event-card__date"><?= $arEvent["DATE_ACTION"] ?></div>
            <div class="event-card__btns">
                <a href="<?= $arEvent["DETAIL_PAGE_URL"] ?>" class="btn btn--default"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_EDIT_EVENT"); ?></a>
                <a href="#" class="btn btn--default disabled"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_CLOSED") ?></a>
                <a href="<?=$arParams["EVENTS_INVITE_URL"]."?EVENT_ID=".$arEvent["ID"]?>" class="btn btn--default"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_REGISTERED", ["#COUNT#" => $arEvent["CNT_ALL"]]); ?></a>
            </div>
            <div class="event-card__statistics">
                <?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_REFUSED", ["#COUNT#" => $arEvent["REFUSED"]]); ?><br>
                <?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_INVITED", ["#COUNT#" => $arEvent["INVITED"]]); ?><br>
            </div>
        </div>
    <? } ?>
</div>
<?
$arParams["IS_AJAX"] = "Y";
$arParams["EVENTS_SHOW_ALL"] = "Y";
?>
<div class="btn btn--default" id="more_past_events" data-params='<?= json_encode($arParams) ?>'><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_LIST_SHOW_ALL"); ?></div>
<!--модалка закрытия события!-->
<div class="modal modal-info fade" id="eventClosePopup" tabindex="-1" role="dialog" aria-labelledby="detailLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="modal-info__close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/sprite.svg#close"/></svg>
                </button>
                <div class="modal-info__content">
                    <h3><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_MODAL_TITLE") ?></h3>
                    <form action="/ajax/events_admin_close_reg.php" method="post" id="reg_close">
                        <input type="hidden" name="event-id" id="event-id">
                        <div class="modal-info__btns">
                            <button class="btn btn--default" type="submit"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_MODAL_OK") ?></button>
                            <button class="btn btn--inverted" data-dismiss="modal" aria-label="Close"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_MODAL_CANCEL") ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


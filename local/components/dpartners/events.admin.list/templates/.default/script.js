$(function () {
    $(document).on("click", "#more_past_events", function () {
        $.ajax({
            type: 'POST',
            url: '/ajax/events_admin_lazyload.php',
            data: {
                'JS_PARAMS': $(this).attr("data-params")
            },
            dataType: 'html',
            success: function (data) {
                $("#past_events_wrap").html(data);
                var scrollTop = $('#past_events_wrap').offset().top;
                $(document).scrollTop(scrollTop);
                $("#more_past_events").remove();
            },
            error: function (e) {
                console.log(e);
            }

        });
    });
    $(document).on("submit", "#reg_close", function (event) {
        var elementID = $(this).find("input[name='event-id']").val();
        if (elementID) {
            $.ajax({
                type: 'POST',
                url: '/ajax/events_admin_close_reg.php',
                data: {
                    'event': elementID
                },
                dataType: 'html',
                success: function (data) {
                      if (data=="OK"){
                          location.reload();
                      }
                },
                error: function (e) {
                    console.log(e);
                }

            });
        }else{
            console.log("Error event ID");
        }

    });   
})



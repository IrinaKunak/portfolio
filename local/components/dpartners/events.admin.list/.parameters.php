<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
$arTypes = [
    "future",
    "past"
];

$arComponentParameters = [
    "PARAMETERS" => [
        "IBLOCK_ID" => [
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("COMP_DP_EVENTS_ADMIN_IBLOCK_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "8",
        ],
        "EVENTS_INVITE_URL" => [
          "PARENT" => "BASE",
            "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_EVENTS_INVITE_URL"),
            "TYPE" => "STRING",
            "DEFAULT" => '/events-invite/'
        ],    
        "EVENTS_PAGE_COUNT" => [
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_PAGE_COUNT"),
            "TYPE" => "STRING",
            "DEFAULT" => 3
        ],
        "EVENTS_SHOW_ALL" => [
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_SHOW_ALL"),
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "off"
        ],
        "SEF_MODE" => [
            "list" => [
                "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_PAGE_LIST"),
                "DEFAULT" => "",
                "VARIABLES" => [],
            ],
            "detail" => [
                "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_PAGE_DETAIL"),
                "DEFAULT" => "#ELEMENT_ID#/",
                "VARIABLES" => ['ELEMENT_ID'],
            ],
        ],
        "CACHE_TIME" => ["DEFAULT" => 36000000],
    ]
];

<?
$MESS["COMP_DP_EVENTS_ADMIN_IBLOCK_ID"] = "ИД инфоблока событий";
$MESS["CP_DP_EVENTS_ADMIN_ELEMENT_ID"] = "Идентификатор элемента";
$MESS["CP_DP_EVENTS_ADMIN_PAGE_LIST"] = "Список";
$MESS["CP_DP_EVENTS_ADMIN_PAGE_DETAIL"] = "Детальная";
$MESS["CP_DP_EVENTS_ADMIN_PAGE_COUNT"] = "Количество событий в блоке";
$MESS["CP_DP_EVENTS_ADMIN_SHOW_ALL"] = "Показать все";
$MESS["COMP_DP_EVENTS_ADMIN_LIST_EVENT_TYPE"] = "Тип событий";
$MESS["CP_DP_EVENTS_ADMIN_EVENTS_INVITE_URL"] = "URL страницы приглашенных";


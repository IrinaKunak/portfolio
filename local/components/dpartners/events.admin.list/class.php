<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader;

class DPartnersAdminEventsList extends CBitrixComponent {

    public function executeComponent() {
        if ($this->arParams["IS_AJAX"] == "Y" && $this->arParams["EVENTS_SHOW_ALL"] == "Y") {
            $this->getPastEvents(true);
            $this->includeComponentTemplate();
        } else {
            $this->getFutureEvents();
            $this->getPastEvents(false);
            $this->includeComponentTemplate();
        }
    }

    public function onPrepareComponentParams($arParams) {
        if ($this->checkModules()) {
            if ((int) $arParams["IBLOCK_ID"] < 1) {
                $arParams["IBLOCK_ID"] = 8;
            } else {
                $arParams["IBLOCK_ID"] = (int) $arParams["IBLOCK_ID"];
            }
            return ($arParams);
        }
    }

    /*
     * 
     * проверка подключенных модулей
     * 
     * @return boolean - результат проверки модулей
     */

    protected function checkModules() {
        $success = true;

        if (!Loader::includeModule('iblock')) {
            $success = false;
            ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALL'));
        }

        if (!Loader::includeModule('form')) {
            $success = false;
            ShowError(Loc::getMessage('IBLOCK_FORM_NOT_INSTALL'));
        }

        return $success;
    }

    /*

     * получить список будущих событий
     * @staticvar arResult
     *  */

    protected function getFutureEvents() {
        $this->arResult["FUTURE_EVENTS"] = [];
        $objDateTimeNow = new DateTime();
        $res = CIBlockElement::GetList(
                        [
                            "ID" => "DESC"
                        ],
                        [
                            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                            "ACTIVE" => "Y",
                            ">=PROPERTY_DATE_ACTION" => $objDateTimeNow->format("Y-m-d") . " 00:00:00",
                        ],
                        false,
                        false,
                        [
                            "ID", "NAME", "PROPERTY_DATE_ACTION", "PROPERTY_DATE_END", "PROPERTY_REG_FORM_ID", "PROPERTY_REG_CLOSE"
                        ]
        );
        while ($arRes = $res->Fetch()) {

            $arFutureEvent = [];
            $arFutureEvent = [
                "NAME" => $arRes["NAME"],
                "ID" => $arRes["ID"],
                "DATE_ACTION" => $arRes["PROPERTY_DATE_ACTION_VALUE"],
                "FORM_ID" => $arRes["PROPERTY_REG_FORM_ID_VALUE"],
                "DETAIL_PAGE_URL" => $this->getDetailUrl($arRes["ID"])
            ];
            if ($arRes["PROPERTY_REG_CLOSE_VALUE"] == "Да") {
                $arFutureEvent["REG_CLOSE"] = true;
            } else {
                $arFutureEvent["REG_CLOSE"] = false;
            }
            if ((int) $arRes["PROPERTY_REG_FORM_ID_VALUE"] > 0) {
                $arFutureEvent["CNT_ALL"] = CFormResult::GetCount($arRes["PROPERTY_REG_FORM_ID_VALUE"]);
                $arOtherResults = $this->getAllStatusesForm((int) $arRes["PROPERTY_REG_FORM_ID_VALUE"]);
                $arFutureEvent["INVITED"] = $arOtherResults["INVITED"];
                $arFutureEvent["REFUSED"] = $arOtherResults["REFUSED"];
            }
            $this->arResult["FUTURE_EVENTS"][] = $arFutureEvent;
        }
    }

    /*

     * получить список прошедших событий
     * @staticvar arResult
     *  */

    protected function getPastEvents(bool $showAll = false) {
        $this->arResult["PAST_EVENTS"] = [];

        if (!$showAll) {
            $navParam = ["nPageSize" => $this->arParams["EVENTS_PAGE_COUNT"]];
        } else {
            $navParam = false;
        }

        $objDateTimeNow = new DateTime();
        $res = CIBlockElement::GetList(
                        [
                            "ID" => "DESC"
                        ],
                        [
                            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                            "ACTIVE" => "Y",
                            "<=PROPERTY_DATE_END" => $objDateTimeNow->format("Y-m-d") . " 00:00:00",
                        ],
                        false,
                        $navParam,
                        [
                            "ID", "NAME", "PROPERTY_DATE_ACTION", "PROPERTY_DATE_END", "PROPERTY_REG_FORM_ID"
                        ]
        );

        while ($arRes = $res->Fetch()) {
            $arPastEvent = [];
            $arPastEvent = [
                "NAME" => $arRes["NAME"],
                "ID" => $arRes["ID"],
                "DATE_ACTION" => $arRes["PROPERTY_DATE_ACTION_VALUE"],
                "FORM_ID" => $arRes["PROPERTY_REG_FORM_ID_VALUE"],
                "DETAIL_PAGE_URL" => $this->getDetailUrl($arRes["ID"])
            ];
            if ((int) $arRes["PROPERTY_REG_FORM_ID_VALUE"] > 0) {
                $arPastEvent["CNT_ALL"] = CFormResult::GetCount($arRes["PROPERTY_REG_FORM_ID_VALUE"]);
                $arOtherResults = $this->getAllStatusesForm((int) $arRes["PROPERTY_REG_FORM_ID_VALUE"]);
                $arPastEvent["INVITED"] = $arOtherResults["INVITED"];
                $arPastEvent["REFUSED"] = $arOtherResults["REFUSED"];
            }
            $this->arResult["PAST_EVENTS"][] = $arPastEvent;
        }
    }

    /*

     * получить количество ответов в статусе приглашен и отказано
     * @param int $form - ИД вебформы
     * @return array - количество ответов в статусе приглашен и отказано    */

    protected function getAllStatusesForm($form) {
        $arStatuses = [];
        $rsStatuses = CFormStatus::GetList(
                        $form,
                        $by = "s_id",
                        $order = "acs",
                        [
                            "ID_EXACT_MATCH" => "Y",
                            "ACTIVE" => "Y",
                        ],
                        $is_filtered
        );
        while ($arStatus = $rsStatuses->Fetch()) {
            if ($arStatus["CSS"] == "invited") {
                $arStatuses["INVITED"] = $arStatus["RESULTS"];
            }
            if ($arStatus["CSS"] == "refused") {
                $arStatuses["REFUSED"] = $arStatus["RESULTS"];
            }
        }

        return $arStatuses;
    }

    /*

     * получить детальный url c ЧПУ
     * @param int - индекс входящего элемента
     * @return element detail URL     */

    protected function getDetailUrl($elementID) {
        if ($this->arParams["SEF_MODE"] == "Y") {
            return $this->arParams["SEF_FOLDER"] . str_replace('#ELEMENT_ID#', $elementID, $this->arParams["SEF_URL_TEMPLATES"]["detail"]);
        } else {
            return $this->arParams["SEF_FOLDER"] . '?ELEMENT_ID=' . $elementID;
        }
    }

}

<?

define("NO_KEEP_STATISTIC", true);
define("NO_AGENT_CHECK", true);
define('PUBLIC_AJAX_MODE', true);
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
$_SESSION["SESS_SHOW_INCLUDE_TIME_EXEC"] = "N";
$APPLICATION->ShowIncludeStat = false;

// Если это не AJAX запрос, выводим ошибку
if (!\Portal\Tools::isAjax()) {
    $result = array(
        "status" => "error",
        "error" => (LANGUAGE_ID == "en" ? "Error! This is not ajax request." : "Ошибка! Это не ajax запрос.")
    );

    echo json_encode($result);
    exit;
}

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

$post = $request->getPostList()->toArray();

$params = json_decode($post["paramsString"], true);


$APPLICATION->IncludeComponent(
        'dpartners:events.admin.form.results',
        "ajax",
        $params
);

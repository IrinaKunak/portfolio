<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;
?>
<!--<form method="POST" data-ajax-url="<?= $componentPath . '/ajax.php' ?>" id="form-result">-->
    <input type="hidden" id="ParamsString" value='<?= json_encode($arParams) ?>'>
    <h2 class="page-title"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_FORM_RESULTS", ["#USERS_COUNT#" => $arResult["USERS_COUNT"]]); ?></h2>
    <div class="register-list__wrap">
        <table class="register-list__detail form" data-api="<?= $componentPath . '/ajax.php' ?>">
            <tr>
                <th colspan="2"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_FORM_RESULTS_HSTATUS") ?></th>
                <? foreach ($arResult["HEADERS"] as $id => $header) { ?>
                    <th data-header="<?= $id ?>">
                        <? if ($header["SORT"] == "Y") { ?>
                            <div class="register-list__dropdown">
                                <?= $header["TITLE"] ?>
                                <a class="register-list__arrow" href="#" role="button" id="dropdownMenuLink2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <svg class="svg"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/sprite.svg#arrow-down"/></svg>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuLink2">
                                    <li class="dropdown-item dropdown-sort-down"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_FORM_SORT1"); ?></li>
                                    <li class="dropdown-item dropdown-sort-up"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_FORM_SORT1"); ?></li>
                                    <? if ($header["FILTER_DATA"] && !empty($header["FILTER_DATA"])) { ?>
                                        <? foreach ($header["FILTER_DATA"] as $filter) {
                                            ?>
                                            <li class="dropdown-item dropdown-filter" data-filter-sid="<?= $arResult["QUESTIONS"][$id]["SID"] ?>"><?= $filter["MESSAGE"] ?></li>
                                        <? } ?>
                                    <? } ?>
                                </ul>
                            </div>
                        <? } else { ?>
                            <?= $header["TITLE"] ?>
                        <? } ?>
                    </th>
                <? } ?>
            </tr>
            <? foreach ($arResult["arrResults"] as $resultID => $arAnswers) { ?>

                <tr data-result-id="<?= $resultID ?>">
                    <td>
                        <div class="form-group form-group--checkbox">
                            <label class="form-checkbox">
                                <input class="js-status-check" name="RESULT[<?= $resultID ?>]" type="checkbox">
                                <span class="form-checkbox__text"></span>
                            </label>
                        </div>
                    </td>
                    <td>
                        <select class="js-table-select" name="status[<?= $resultID ?>]" style="width: 100%">
                            <? foreach ($arResult["STATUSES"] as $name => $status) { ?>
                                <option value="<?= $name ?>" <? if ($arAnswers["STATUS_ID"] == $name) { ?>selected<? } ?>><?= $status ?></option>
                            <? } ?>
                        </select>
                    </td>  
                    <? foreach ($arResult["HEADERS"] as $id => $header) { ?>
                        <? if ($arAnswers["ANSWERS"][$id]) { ?>
                            <td><?= $arAnswers["ANSWERS"][$id]["TITLE"] ?></td>
                        <? } else { ?>
                            <td></td>
                        <? } ?>
                    <? } ?>
                </tr>
            <? } ?>
        </table>
    </div>

    <div class="register-list__btns hidden">
        <select class="js-table-select set-all-select-status" name="ALL_STATUS" style="width: 100%">
            <? foreach ($arResult["STATUSES"] as $name => $status) { ?>
                <option value="<?= $name ?>"><?= $status ?></option>
            <? } ?>
        </select>    
        <button class="btn btn--default" type="submit"><?= Loc::getMessage("CP_DP_EVENTS_ADMIN_FORM_RESULTS_INVITE"); ?></button>
    </div>
    <?=$arResult["NAV_STRING"];?>
<!--</form>-->


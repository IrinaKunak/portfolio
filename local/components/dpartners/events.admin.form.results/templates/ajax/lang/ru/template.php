<?php
$MESS["CP_DP_EVENTS_ADMIN_FORM_RESULTS"] = "Список зарегистрированных на мероприятие: #USERS_COUNT# участника (ов)";
$MESS["CP_DP_EVENTS_ADMIN_FORM_RESULTS_INVITE"] = "Установить для выбранных";
$MESS["CP_DP_EVENTS_ADMIN_FORM_RESULTS_REFUSE"] = "Отказать всем выбранным";
$MESS["CP_DP_EVENTS_ADMIN_FORM_RESULTS_HSTATUS"] = "Статус заявки";
$MESS["CP_DP_EVENTS_ADMIN_FORM_GO_BACK"] = "вернуться назад";
$MESS["CP_DP_EVENTS_ADMIN_FORM_SORT1"] = "От А до Я";
$MESS["CP_DP_EVENTS_ADMIN_FORM_SORT2"] = "От Я до А";


$(function () {
    $(document).on("click", ".dropdown-sort-up", function () {
        var sortField = $(this).parents("th").attr('data-header');
        console.log(sortField);
        setSessionParam("BX_EVENT_ADMIN_SORT_FIELD", sortField);
        setSessionParam("BX_EVENT_ADMIN_SORT_ORDER", "desc");
        console.log(getSessionParam("BX_EVENT_ADMIN_SORT_FIELD"));
        console.log(getSessionParam("BX_EVENT_ADMIN_SORT_ORDER"));

        location.reload();
    });
    $(document).on("click", ".dropdown-sort-down", function () {
        var sortField = $(this).parents("th").attr('data-header');
//        console.log(sortField);
        setSessionParam("BX_EVENT_ADMIN_SORT_FIELD", sortField);
        setSessionParam("BX_EVENT_ADMIN_SORT_ORDER", "asc");
//        console.log(getSessionParam("BX_EVENT_ADMIN_SORT_FIELD"));
//        console.log(getSessionParam("BX_EVENT_ADMIN_SORT_ORDER"));

        location.reload();
//        setTimeout(function () {
//            location.reload();
//        }, 300);
    });
    $(document).on("click", ".dropdown-filter", function () {
        var
                question = $(this).attr("data-filter-sid"),
                answer = $(this).text();

        setSessionParam("BX_EVENT_ADMIN_FILTER_FIELD", question);
        setSessionParam("BX_EVENT_ADMIN_FILTER_ANSWER", answer);

        location.reload();
    });
    $(document).on("click", ".dropdown-filter-status", function () {
        var
                question = "STATUS",
                answer = $(this).attr("data-filter-sid");

        setSessionParam("BX_EVENT_ADMIN_FILTER_STATUS", question);
        setSessionParam("BX_EVENT_ADMIN_FILTER_STATUS_ID", answer);

        location.reload();
    });
    $(document).on("click", ".reset-filters", function () {
        setSessionParam("BX_EVENT_ADMIN_FILTER_FIELD", "");
        setSessionParam("BX_EVENT_ADMIN_FILTER_ANSWER", "");
        setSessionParam("BX_EVENT_ADMIN_FILTER_STATUS", "");
        setSessionParam("BX_EVENT_ADMIN_FILTER_STATUS_ID", "");
        setSessionParam("BX_EVENT_ADMIN_SORT_FIELD", "");
        setSessionParam("BX_EVENT_ADMIN_SORT_ORDER", "");

//        location.reload();
        setTimeout(function () {
            location.reload();
        }, 300);
    });
});



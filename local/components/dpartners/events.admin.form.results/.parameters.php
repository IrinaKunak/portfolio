<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;

$arComponentParameters = [
    "PARAMETERS" => [
        "ELEMENT_ID" => [
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("COMP_DP_EVENTS_ADMIN_ELEMENT_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ],
        "COUNT_ON_PAGE" => [
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("COMP_DP_EVENTS_ADMIN_COUNT_ON_PAGE"),
            "TYPE" => "STRING",
            "DEFAULT" => "",
        ],
        "CACHE_TIME" => ["DEFAULT" => 36000000],
    ]
];

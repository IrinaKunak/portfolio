<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arComponentDescription = array(
    "NAME" => GetMessage("COMP_DP_EVENTS_ADMIN_FORM_RESULTS"),
    "DESCRIPTION" => GetMessage("COMP_DP_EVENTS_ADMIN_FORM_RESULTS_DESCR"),
    "PATH" => array(
        "ID" => "dpartners",
    ),
);
?>
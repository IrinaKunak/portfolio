<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Engine\Contract\Controllerable,
    \Bitrix\Main\Engine\ActionFilter;

class EventsAdminFormResults extends \CBitrixComponent implements Controllerable {
    /* сопоставление верстки и типов в админке */

    protected static $arTypes = [
        "text-input" => "text",
        "select" => "dropdown",
        "email" => "email",
        "checkbox" => "checkbox",
        "radio" => "radio",
        "textarea" => "textarea",
        "file" => "file",
    ];

    /* статусы для клонирования */
    protected static $arStatuses = [
        "new" => 8,
        "wait" => 9,
        "invited" => 10,
        "refused" => 11,
        "come" => 12,
        "not_come" => 13
    ];
    protected static $sort_field;

    public function onPrepareComponentParams($arParams) {
        if ($this->checkModules()) {
            $arParams["ELEMENT_ID"] = (int) $arParams["ELEMENT_ID"];
            $arParams["COUNT_ON_PAGE"] = (int) $arParams["COUNT_ON_PAGE"];
            if ($arParams["COUNT_ON_PAGE"] < 1)
                $arParams["COUNT_ON_PAGE"] = 20;

            if (!$arParams["SORT"])
                $arParams["SORT"] = "C_SORT";

            if (!$arParams["ORDER"])
                $arParams["ORDER"] = "asc";

            return ($arParams);
        }
    }

    public function executeComponent() {
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        if ($request->isPost() && !\Portal\Tools::isAjax()) {
            $post = $request->getPostList()->toArray();
            $this->changeStatuses($post);
        }
        $this->getResults();
        $this->includeComponentTemplate();
    }

    /*
     * 
     * проверка подключенных модулей
     * 
     * @return boolean - результат проверки модулей
     */

    protected function checkModules() {
        $success = true;

        if (!Loader::includeModule('iblock')) {
            $success = false;
            ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALL'));
        }

        if (!Loader::includeModule('form')) {
            $success = false;
            ShowError(Loc::getMessage('IBLOCK_FORM_NOT_INSTALL'));
        }

        return $success;
    }

    /*
     * 
     * обязательный метод для работы runComponentAjax
     * 
     * @return array - фильтров входящих данных
     */

    public function configureActions() {
        return [
            "getNavigation" => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                            array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    )
                ]
            ],
            "changeStatus" => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                            array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    )
                ]
            ]
        ];
    }

    /*

     * метод изменение статуса регистрации
     * @param array $post - массив результата
     * @return string    */

    public function changeStatusAction($post) {
        $status = "OK";
        $text = "";
        $resultID = preg_replace('/[^0-9]/', '', $post["resultID"]);
        if (CFormResult::SetStatus((int) $resultID, (int) $post["statusVal"])) {
            
        } else {
            global $strError;
            $status = "error";
            $text = $strError;
        }

        return [
            "status" => $status,
            "error" => $text
        ];
    }

    /*

     * метод изменение статуса регистрации
     * @param array $post - массив результата
     * @return string    */

    public function changeStatuses($post) {
        foreach ($post["RESULT"] as $result => $value) {
            CFormResult::SetStatus($result, $post["ALL_STATUS"]);
        }
    }

    /*

     * получить данные события - ИД формы, ссылку на регистрацию, признак завершения регистрации
     * @var  arParams, arResult
     *      */

    public function getEventData() {
        $dbRes = CIBlockElement::GetList(
                        [],
                        [
                            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                            "ID" => $this->arParams["ELEMENT_ID"]
                        ],
                        false,
                        false,
                        ["ID", "IBLOCK_ID", "PROPERTY_REG_FORM_ID"]
        );
        if ($arRes = $dbRes->Fetch()) {
            $this->arResult["EVENT"] = [
                "ID" => $arRes["ID"],
                "REG_FORM_ID" => $arRes["PROPERTY_REG_FORM_ID_VALUE"]
            ];
        }
    }

    public function getResults() {
        $this->getEventData();
        if ($this->arResult["EVENT"]["REG_FORM_ID"]) {
            $this->getUsersCount();
            $this->getResultTableHead();
            $this->getUsersList();
        } else {
            $this->arResult["NO_FORM"] = true;
        }
    }

    /*

     * общее количество зарегистрированных (заполненных форм)
     * @var arResult
     *  */

    protected function getUsersCount() {
        $this->arResult["USERS_COUNT"] = CFormResult::GetCount($this->arResult["EVENT"]["REG_FORM_ID"]);
    }

    /*

     * получить все ответы на вопросы по ИД результата 
     * @param integer $resultID - результат формы
     * @var arResult
     * @return array массив ответов пользователя
     * 
     *  */

    protected function getResultAnswers($resultID) {
        $arAnswers = [];
        \CForm::GetResultAnswerArray($this->arResult["EVENT"]["REG_FORM_ID"],
                $arrColumns,
                $arrAnswers,
                $arrAnswersVarname,
                ["RESULT_ID" => $resultID]);

        foreach ($arrColumns as $column) {
            $arAnswers[$column["ID"]] = [
                "SID" => $column["SID"],
                "ID" => $column["ID"],
                "TITLE" => $column["TITLE"],
                "ANSWERS" => []
            ];
        }

        foreach ($arrAnswers[$resultID] as $id => $asAnswer) {
            if (count($asAnswer) > 1) {
                $arAnswerQuestions[$id]["ID"] = "";
                unset($fieldType);
                foreach ($asAnswer as $answ) {
                    unset($textResult);
                    if (!$fieldType)
                        $fieldType = $answ["FIELD_TYPE"];

                    if ($answ["USER_TEXT"]) {
                        $textResult = $answ["USER_TEXT"];
                    } else {
                        if ($fieldType == $answ["FIELD_TYPE"])
                            $textResult = $answ["MESSAGE"];
                    }
                    $arAnswers[$id]["TITLE"] .= '; ' . $textResult;
                }
            } else {
                $firstAnswer = array_shift($asAnswer);
                switch ($firstAnswer["FIELD_TYPE"]) {
                    case "file":
                        if ($firstAnswer["USER_FILE_ID"]) {
                            $arAnswers[$id]["ID"] = $firstAnswer["RESULT_ID"];
                            $arFile = \CFile::GetFileArray($firstAnswer["USER_FILE_ID"]);
                            $arAnswers[$id]["TITLE"] = "<a href='" . $arFile["SRC"] . "'>" . $firstAnswer["USER_FILE_NAME"] . "</a>";
                        }
                        break;
                    default:
                        $arAnswers[$id]["ID"] = $firstAnswer["RESULT_ID"];
                        $arAnswers[$id]["TITLE"] = $firstAnswer["USER_TEXT"] ? $firstAnswer["USER_TEXT"] : $firstAnswer["MESSAGE"];
                        break;
                }
            }
        }
        return $arAnswers;
    }

    /*

     * получить заголовки таблицы результатов
     * @var arResult
     * 
     *  */

    protected function getResultTableHead() {
        $arHeaders = [];

        $rsStatuses = CFormStatus::GetList(
                        $this->arResult["EVENT"]["REG_FORM_ID"],
                        $by = "s_id",
                        $order = "asc",
                        ["ACTIVE" => "Y"],
                        $is_filtered
        );
        while ($arStatus = $rsStatuses->Fetch()) {
            $this->arResult["STATUSES"][$arStatus["ID"]] = $arStatus["TITLE"];
        }

        $rsQuestions = CFormField::GetList(
                        $this->arResult["EVENT"]["REG_FORM_ID"],
                        "ALL",
                        $by = "s_sort",
                        $order = "asc",
                        ["ACTIVE" => "Y"],
                        $is_filtered
        );
        while ($arQuestion = $rsQuestions->Fetch()) {
            unset($arNewQuestion);
            $arNewQuestion = [
                "ID" => $arQuestion["ID"],
                "TITLE" => $arQuestion["TITLE"],
                "SID" => $arQuestion["SID"]
            ];
            $arHeaders[$arQuestion["ID"]]["TITLE"] = $arQuestion["TITLE"];
            $rsAnswers = CFormAnswer::GetList(
                            $arQuestion["ID"],
                            $by = "s_sort",
                            $order = "asc",
                            [
                                "ACTIVE" => "Y"
                            ],
                            $is_filtered
            );
            while ($arAnswer = $rsAnswers->Fetch()) {
                if (!$arNewQuestion["TYPE"])
                    $arNewQuestion["TYPE"] = $arAnswer["FIELD_TYPE"];
                $arNewQuestion["ANSWERS"][] = [
                    "ID" => $arAnswer["ID"],
                    "MESSAGE" => $arAnswer["MESSAGE"],
                    "SORT" => $arAnswer["C_SORT"]
                ];
            }
            $this->arResult["QUESTIONS"][$arQuestion["ID"]] = $arNewQuestion;
        }

        foreach ($arHeaders as $id => $head) {
            switch ($this->arResult["QUESTIONS"][$id]["TYPE"]) {
                case "text": case "email":
                    //только сортировка
                    $arHeaders[$id]["SORT"] = "Y";
                    break;
                case "dropdown": case "radio":
                    //сортировка и фильтрация
                    $arHeaders[$id]["SORT"] = "Y";
                    $arHeaders[$id]["FILTER_DATA"] = $this->arResult["QUESTIONS"][$id]["ANSWERS"];
                    break;
            }
        }
        $this->arResult["HEADERS"] = $arHeaders;
    }

    protected static function compare($a, $b) {
        return strcasecmp($a["ANSWERS"][self::$sort_field]["TITLE"], $b["ANSWERS"][self::$sort_field]["TITLE"]);
    }

    protected static function compareRevers($a, $b) {
        $result = 0;
        if (strcasecmp($a["ANSWERS"][self::$sort_field]["TITLE"], $b["ANSWERS"][self::$sort_field]["TITLE"]) > 0)
            $result = -1;
        if (strcasecmp($a["ANSWERS"][self::$sort_field]["TITLE"], $b["ANSWERS"][self::$sort_field]["TITLE"]) < 0)
            $result = 1;
        return $result;
    }

    /*

     * получить все результаты формы
     * @var arResult
     * @var arParams
     * 
     *  */

    protected function getUsersList() {
        $arFilter = [];

        if ($_SESSION["BX_EVENT_ADMIN_FILTER_STATUS"] && $_SESSION["BX_EVENT_ADMIN_FILTER_STATUS_ID"]) {
            $arFilter = [
                "STATUS_ID" => $_SESSION["BX_EVENT_ADMIN_FILTER_STATUS_ID"]
            ];
            $this->arResult["FILTERED"]["STATUS"] = $_SESSION["BX_EVENT_ADMIN_FILTER_STATUS_ID"];
        };

        if ($_SESSION["BX_EVENT_ADMIN_FILTER_FIELD"] && $_SESSION["BX_EVENT_ADMIN_FILTER_ANSWER"]) {
            $arFields[0] = [
                "SID" => $_SESSION["BX_EVENT_ADMIN_FILTER_FIELD"],
                "PARAMETER_NAME" => "ANSWER_TEXT",
                "VALUE" => $_SESSION["BX_EVENT_ADMIN_FILTER_ANSWER"],
            ];
            $this->arResult["FILTERED"]["FIELD"] = $_SESSION["BX_EVENT_ADMIN_FILTER_FIELD"];
            $this->arResult["FILTERED"]["ANSWER"] = $_SESSION["BX_EVENT_ADMIN_FILTER_ANSWER"];
            $arFilter["FIELDS"] = $arFields;
        };

        $rsResults = CFormResult::GetList(
                        $this->arResult["EVENT"]["REG_FORM_ID"],
                        ($by = "s_timestamp"),
                        ($order = "desc"),
                        $arFilter,
                        $is_filtered
        );
        while ($arFormResult = $rsResults->Fetch()) {
            $this->arResult["RESULTS"][$arFormResult["ID"]] = [
                'ID' => $arFormResult["ID"],
                'STATUS_ID' => $arFormResult["STATUS_ID"],
                'DATE_CREATE' => $arFormResult["DATE_CREATE"],
                'STATUS_TITLE' => $arFormResult["STATUS_TITLE"]
            ];
            $this->arResult["RESULTS"][$arFormResult["ID"]]["ANSWERS"] = $this->getResultAnswers($arFormResult["ID"]);
        }

        if ($_SESSION["BX_EVENT_ADMIN_SORT_FIELD"]) {
            self::$sort_field = $_SESSION["BX_EVENT_ADMIN_SORT_FIELD"];
            $this->arResult["SORT"]["FIELD"] = $_SESSION["BX_EVENT_ADMIN_SORT_FIELD"];
            if ($_SESSION["BX_EVENT_ADMIN_SORT_ORDER"] == "desc") {
                usort($this->arResult["RESULTS"], ["EventsAdminFormResults", "compareRevers"]);
                $this->arResult["SORT"]["ORDER"] = "desc";
            } else {
                usort($this->arResult["RESULTS"], ["EventsAdminFormResults", "compare"]);
                $this->arResult["SORT"]["ORDER"] = "asc";
            }
        }

        CPageOption::SetOptionString("main", "nav_page_in_session", "N");
        $rs = new CDBResult;
        $rs->InitFromArray($this->arResult["RESULTS"]);

        $rs->NavStart(10);

        if ($rs->IsNavPrint()) {
            $template_path = "/local/templates/events_admin/components/bitrix/system.pagenavigation/results/template.php";
            $this->arResult["NAV_STRING"] = $rs->GetNavPrint("Элементы", false, "text", $template_path);
        }
        $pagen_from = (intval($rs->NavPageNomer) - 1) * intval($rs->NavPageSize);
        while ($arRes = $rs->NavNext(false)) {
            $this->arResult["arrResults"][] = $arRes;
        }
    }

}

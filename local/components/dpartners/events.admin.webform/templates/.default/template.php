<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc;
?>
<?
//\Bitrix\Main\Diag\Debug::dump($arResult["STATUSES_VALUE"]);
?>
<form action="" class="form" method="POST" id="formEditForm">
    <input type="hidden" name="EVENT_ID" value="<?= $arResult["EVENT"]["ID"] ?>">
    <input type="hidden" name="FORM_ID" value="<?= $arResult["EVENT"]["REG_FORM_ID"] ?>">
    <h2 class="page-title"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TITLE"); ?></h2>
    <div class="event__wrap">
        <div class="event__row">
            <div class="event__note"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_OUT_LINK"); ?></div>
            <div class="event__control">
                <div class="form-group">
                    <input class="form-control" type="text" placeholder="" name="REG_LINK" value="<?= $arResult["EVENT"]["REG_LINK"] ?>">
                </div>
            </div>
        </div>
        <div class="event__row">
            <div class="form-group form-group--checkbox">
                <label class="form-checkbox">
                    <input name="REG_CLOSE" type="checkbox" <? if ($arResult["EVENT"]["REG_CLOSE"]) { ?>checked<? } ?>>
                    <span class="form-checkbox__text"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_REG_CLOSE"); ?></span>
                </label>
            </div>
        </div>
    </div>
    <h2 class="page-title"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TITLE2"); ?></h2>
    <div class="event__wrap">
        <? foreach ($arResult["QUESTIONS"] as $ind => $question) { ?>
            <div class="event__question">
                <div class="event__top">
                    <h3 class="event__title"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION") ?> <?= $ind + 1 ?></h3>
                    <button type="button" class="event__delete js-remove-question" data-question-id="<?= $question['ID'] ?>">
                        <svg class="svg"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/sprite.svg#close"/></svg>
                    </button>
                </div>                
                <div class="event__row">
                    <div class="form-group form-group--checkbox">
                        <label class="form-checkbox">
                            <input type="checkbox" name="QUESTION[<?= $question['ID'] ?>][REQUIRED]" <? if ($question["REQUIRED"] == 'Y') { ?>checked value="on"<? } ?>>
                            <span class="form-checkbox__text"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_REQUIRED") ?></span>
                        </label>
                    </div>
                </div>
                <div class="event__row">
                    <div class="event__note"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_SORT") ?></div>
                    <div class="event__control">
                        <div class="form-group">
                            <input class="form-control form-control--sm" name="QUESTION[<?= $question['ID'] ?>][C_SORT]" type="number" value="<?= $question['C_SORT'] ?>" placeholder="">
                        </div>
                    </div>
                </div>                
                <div class="event__row">
                    <div class="event__note"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_TEXT") ?></div>
                    <div class="event__control">
                        <div class="form-group">
                            <input class="form-control" name="QUESTION[<?= $question['ID'] ?>][TITLE]" type="text" placeholder="" value="<?= $question["TITLE"] ?>">                            
                        </div>
                    </div>
                </div>
                <? // \Bitrix\Main\Diag\Debug::dump($question["TYPE"]);?>
                <div class="event__row">
                    <div class="event__note"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_TYPE") ?></div>
                    <div class="event__control">
                        <div class="form-group">
                            <select class="js-select js-select-single js-type-change" name="QUESTION[<?= $question['ID'] ?>][TYPE]" multiple="multiple" style="width: 100%">
                                <option value="text-input" <? if ($question["TYPE"] == "text") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_TEXT") ?></option>
                                <option value="email" <? if ($question["TYPE"] == "email") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_EMAIL") ?></option>
                                <option value="checkbox" <? if ($question["TYPE"] == "checkbox") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_CHECKBOX") ?></option>
                                <option value="radio" <? if ($question["TYPE"] == "radio") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_RADIO") ?></option>
                                <option value="textarea" <? if ($question["TYPE"] == "textarea") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_TEXTAREA") ?></option>
                                <option value="select" <? if ($question["TYPE"] == "dropdown") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_SELECT") ?></option>
                                <option value="file" <? if ($question["TYPE"] == "file") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_FILE") ?></option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="event__answers <? if (in_array($question["TYPE"], ["text", "email", "textarea", "file"])) { ?> hidden <? } ?>">
                    <? foreach ($question["ANSWERS"] as $answer) { ?>
                        <div class="event__row">
                            <div class="event__note"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_ANSWER") ?></div>
                            <div class="event__control">
                                <!--                                <div class="form-group sort-input">
                                                                    <input class="form-control" name="" type="text" name="answer-1-1" placeholder="" value="<?= $answer["SORT"] ?>">
                                                                </div>-->
                                <div class="form-group">
                                    <input class="form-control" name="ANSWER[<?= $question['ID'] ?>][<?= $answer["ID"] ?>]" type="text" placeholder="" value="<?= $answer["MESSAGE"] ?>">
                                </div>
                                <button type="button" class="event__delete js-remove-answer" data-question-id="<?= $question['ID'] ?>" data-answer-id="<?= $answer['ID'] ?>"><svg class="svg"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/sprite.svg#close"></use></svg></button>
                            </div>
                        </div>
                    <? } ?>
                    <div class="event__add-btn">
                        <button class="btn btn--default js-add-answer" type="button" data-question="<?= $question['ID'] ?>" data-answer="1"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_ANSWER_ADD") ?></button>
                    </div>
                </div>
            </div>
        <? } ?>
        <script id="question-template" type="x-tmpl-mustache">
            <div class="event__question">
                <div class="event__top">
                    <h3 class="event__title"> <?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION") ?> {{index}}</h3>
                            <button type="button" class="event__delete js-remove-question">
                                <svg class="svg"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/sprite.svg#close"/></svg>
                                        </button>
                                    </div>
                                    <div class="event__row">
                                        <div class="form-group form-group--checkbox">
                                            <label class="form-checkbox">
                                                <input type="checkbox" name="QUESTION_NEW[{{index}}][REQUIRED]">
                                                    <span class="form-checkbox__text"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_REQUIRED") ?></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="event__row">
                                            <div class="event__note"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_SORT") ?></div>
                                        <div class="event__control">
                                            <div class="form-group">
                                                <input class="form-control form-control--sm" name="QUESTION_NEW[{{index}}][C_SORT]" type="number" value="" placeholder="">
                                                    </div>
                                                </div>
                                            </div>              
                                            <div class="event__row">
                                                <div class="event__note"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_TEXT") ?></div>
                                        <div class="event__control">
                                            <div class="form-group">
                                                <input class="form-control" name="QUESTION_NEW[{{index}}][TITLE]" type="text" placeholder="" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="event__row">
                                                <div class="event__note"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_TYPE") ?></div>
                    <div class="event__control">
                        <div class="form-group">
                                        <select class="js-select js-select-single js-type-change" name="QUESTION_NEW[{{index}}][TYPE]" multiple="multiple" style="width: 100%">
                                                <option value="text-input" <? if ($question["TYPE"] == "text") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_TEXT") ?></option>
                                                        <option value="email" <? if ($question["TYPE"] == "email") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_EMAIL") ?></option>
                                                        <option value="checkbox" <? if ($question["TYPE"] == "checkbox") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_CHECKBOX") ?></option>
                                                        <option value="radio" <? if ($question["TYPE"] == "radio") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_RADIO") ?></option>
                                    <option value="textarea" <? if ($question["TYPE"] == "textarea") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_TEXTAREA") ?></option>
                                                        <option value="select" <? if ($question["TYPE"] == "dropdown") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_SELECT") ?></option>
                                                        <option value="file" <? if ($question["TYPE"] == "file") { ?>selected<? } ?>><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_TYPE_FILE") ?></option>
                                                    </select>
                                                    </div>
                                                    </div>
                                                    </div>
                                                    <div class="event__answers hidden">
                                                        <div class="event__row">
                                                            <div class="event__note"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_ANSWER") ?></i></div>
                                                            <div class="event__control">
                                                                <div class="form-group">
                                                                    <input class="form-control" type="text" name="answer[{{index}}][1]" placeholder="">
                            </div>
                        </div>
                    </div>
                    <div class="event__add-btn">
                                                                           <button class="btn btn--default js-add-answer" type="button" data-question="{{index}}" data-answer="2"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_ANSWER_ADD") ?></button>
                    </div>
                </div>
            </div>

                                                                            </script>
                                                                                <div class="questions" id="questions-target" data-index="1"></div>
                                                                                <div class="event__add-btn">
                                                                                    <button type="button" class="btn btn--inverted js-add-question"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_ADD") ?></button>
                                                                                </div>
                                                                        </div>
                                                                        <h2 class="page-title"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_STATUS_INVITE") ?></h2>
                                                                        <div class="event__wrap">
                                                                            <? foreach ($arResult["STATUSES"]["event"] as $name => $status) { ?>
                                                                                <div class="event__row">
                                                                                    <div class="form-group form-group--checkbox">
                                                                                        <label class="form-checkbox">
                                                                                            <? if ($name == "new") { ?>
                                                                                                <input name="STATUS_EVENT[]" type="checkbox" value="<?= $name ?>" checked disabled="">
                                                                                            <? } elseif ($arResult["STATUSES_VALUE"][$name] && $arResult["STATUSES_VALUE"][$name]["ACTIVE"] == 'Y') { ?>
                                                                                                <input name="STATUS_EVENT[]" type="checkbox" value="<?= $name ?>" checked>
                                                                                            <? } else { ?>
                                                                                                <input name="STATUS_EVENT[]" type="checkbox" value="<?= $name ?>">
                                                                                            <? } ?>                    
                                                                                            <span class="form-checkbox__text"><?= $status ?></span>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            <? } ?>
                                                                        </div>

                                                                        <h2 class="page-title"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_STATUS_VISIT") ?></h2>
                                                                        <div class="event__wrap">
                                                                            <? foreach ($arResult["STATUSES"]["visit"] as $name => $status) { ?>
                                                                                <div class="event__row">
                                                                                    <div class="form-group form-group--checkbox">
                                                                                        <label class="form-checkbox">
                                                                                            <? if ($arResult["STATUSES_VALUE"][$name] && $arResult["STATUSES_VALUE"][$name]["ACTIVE"] == 'Y') { ?>
                                                                                                <input name="STATUS_EVENT[]" type="checkbox" value="<?= $name ?>" checked>
                                                                                            <? } else { ?>
                                                                                                <input name="STATUS_EVENT[]" type="checkbox" value="<?= $name ?>">
                                                                                            <? } ?>
                                                                                            <span class="form-checkbox__text"><?= $status ?></span>
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            <? } ?>
                                                                        </div>
                                                                        <div class="event__btn">
                                                                            <? if (!$arResult["EVENT"]["ID"]) { ?>
                                                                                <button class="btn btn--default" type="submit" disabled><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_SAVE") ?></button>
                                                                            <? } else { ?>
                                                                                <button class="btn btn--default" type="submit"><?= Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_SAVE") ?></button>
                                                                            <? } ?>
                                                                        </div>
                                                                        </form>
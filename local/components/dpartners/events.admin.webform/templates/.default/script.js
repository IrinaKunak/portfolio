$(function () {
    $(document).on("change", "input[name='REG_LINK']", function () {
        var value = $(this).val();
        var elID = $(this).parents("form.form").find("input[name='EVENT_ID']").val();

        BX.ajax.runComponentAction('dpartners:events.admin.webform', 'updateRegLink', {
            mode: 'class',
            data: {
                post: {
                    action: "editUrl",
                    value: value,
                    elementID: elID
                }
            }
        }).then(function (response) {
            //console.log(response)
        }).catch(function (response) {
            console.log(response)
            $("input[name='REG_LINK']").after('<div class="error red">Что-то пошло не так. Изменения не сохранены</div>');
        });
    });
    $(document).on("change", "input[name='REG_CLOSE']", function () {
        var value = false;

        if ($(this).is(':checked'))
            value = true;

        var elID = $(this).parents("form.form").find("input[name='EVENT_ID']").val();

        BX.ajax.runComponentAction('dpartners:events.admin.webform', 'updateRegLink', {
            mode: 'class',
            data: {
                post: {
                    action: "reg_close",
                    value: value,
                    elementID: elID
                }
            }
        }).then(function (response) {
            //console.log(response)
        }).catch(function (response) {
            console.log(response)
            $("input[name='REG_CLOSE']").after('<div class="error red">Что-то пошло не так. Изменения не сохранены</div>');
        });
    });
    $(document).on("submit", "#formEditForm", function (e) {
        e.preventDefault();
        var post = $(this).serializeArray();
        var form = $(this);
        BX.ajax.runComponentAction('dpartners:events.admin.webform', 'editForm', {
            mode: 'class',
            data: {
                post: post
            }
        }).then(function (response) {
            console.log(response);
            if (response.data.status == "OK") {
                setSessionParam("BX_EVENT_ADMIN_ACTIVE_TAB", 'register');
               // location.reload();
            } else {
                form.prepend('<div class="error red">' + response.data.error + '</div>');

                var scrollTop = $('.error').offset().top;
                $(document).scrollTop(scrollTop);
            }
        }).catch(function (response) {
            console.log(response);
        });
    });
    $(document).on("click", ".js-remove-answer", function (e) {
        var questionID = $(this).attr("data-question-id");
        var answerID = $(this).attr("data-answer-id");
        if (typeof questionID !== 'undefined' && typeof answerID !== 'undefined') {
            BX.ajax.runComponentAction('dpartners:events.admin.webform', 'deleteAnswer', {
                mode: 'class',
                data: {
                    post: {
                        questionID: questionID,
                        answerID: answerID
                    }
                }
            }).then(function (response) {
                console.log(response);
            }).catch(function (response) {
                console.log(response);
            });
        }
    });
    $(document).on("click", ".js-remove-question", function (e) {
        var questionID = $(this).attr("data-question-id"),
                remove_question = $(this);
        console.log("delete");
        console.log(questionID);
        if (typeof questionID !== 'undefined') {
            BX.ajax.runComponentAction('dpartners:events.admin.webform', 'deleteQuestion', {
                mode: 'class',
                data: {
                    post: {
                        questionID: questionID
                    }
                }
            }).then(function (response) {
                if (response.data.status == "error") {
                    remove_question.prev().append('<div class="error red">' + JSON.parse(response.data.error) + '</div>');
                } else {
                    remove_question.parents(".event__question").detach();
                }
            }).catch(function (response) {
                console.log(response);
            });
        }
    });

})



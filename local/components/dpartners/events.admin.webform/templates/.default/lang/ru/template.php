<?php

$MESS["CD_DP_EVENT_ADMIN_FORM_TITLE"] = "Регистрация на событие";
$MESS["CD_DP_EVENT_ADMIN_FORM_TITLE2"] = "Форма регистрации";
$MESS["CD_DP_EVENT_ADMIN_FORM_OUT_LINK"] = "Внешняя ссылка <br><i>(если пустая - не отображается)</i>";
$MESS["CD_DP_EVENT_ADMIN_FORM_REG_CLOSE"] = "Регистрация закрыта";
$MESS["CD_DP_EVENT_ADMIN_FORM_QUESTION"] = "Вопрос";
$MESS["CD_DP_EVENT_ADMIN_FORM_QUESTION_TEXT"] = "Текст вопроса";
$MESS["CD_DP_EVENT_ADMIN_FORM_QUESTION_TYPE"] = "Тип вопроса";
$MESS["CD_DP_EVENT_ADMIN_FORM_TYPE_TEXT"] = "Поле ввода";
$MESS["CD_DP_EVENT_ADMIN_FORM_TYPE_EMAIL"] = "Email";
$MESS["CD_DP_EVENT_ADMIN_FORM_TYPE_CHECKBOX"] = "Чекбокс";
$MESS["CD_DP_EVENT_ADMIN_FORM_TYPE_RADIO"] = "Радиобаттон";
$MESS["CD_DP_EVENT_ADMIN_FORM_TYPE_TEXTAREA"] = "Поле ввода длинного текста";
$MESS["CD_DP_EVENT_ADMIN_FORM_TYPE_SELECT"] = "Выпадающий список";
$MESS["CD_DP_EVENT_ADMIN_FORM_TYPE_FILE"] = "Файл";
$MESS["CD_DP_EVENT_ADMIN_FORM_ANSWER"] = "Вариант ответа <br><i>(для вопросов с выбором ответа)</i>";
$MESS["CD_DP_EVENT_ADMIN_FORM_ANSWER_ADD"] = "Добавить вариант ответа";
$MESS["CD_DP_EVENT_ADMIN_FORM_QUESTION_ADD"] = "Добавить вопрос";
$MESS["CD_DP_EVENT_ADMIN_FORM_STATUS_INVITE"] = "Статусы приглашений пользователей";
$MESS["CD_DP_EVENT_ADMIN_FORM_STATUS_VISIT"] = "Статусы визита пользователей";
$MESS["CD_DP_EVENT_ADMIN_FORM_SAVE"] = "Сохранить";
$MESS["CD_DP_EVENT_ADMIN_FORM_REQUIRED"] = "Обязательный";
$MESS["CD_DP_EVENT_ADMIN_FORM_SORT"] = "Индекс сортировки";


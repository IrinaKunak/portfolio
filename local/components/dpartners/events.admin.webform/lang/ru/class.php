<?php
$MESS["CD_DP_EVENT_ADMIN_FORM_STATUS1"] = "Зарегистрировался";
$MESS["CD_DP_EVENT_ADMIN_FORM_STATUS2"] = "Приглашен";
$MESS["CD_DP_EVENT_ADMIN_FORM_STATUS3"] = "Отклонен";
$MESS["CD_DP_EVENT_ADMIN_FORM_STATUS4"] = "На рассмотрении";
$MESS["CD_DP_EVENT_ADMIN_FORM_STATUS5"] = "Пришел";
$MESS["CD_DP_EVENT_ADMIN_FORM_STATUS6"] = "Не пришел";
$MESS["CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR"] = "Произошла ошибка при обновлении вопроса #ID#";
$MESS["CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR2"] = "Произошла ошибка при смене типа вопроса #ID#";
$MESS["CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR3"] = "Произошла ошибка при добавлении вопроса #ID#";
$MESS["CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR4"] = "Произошла ошибка при добавлении ответа на вопрос #ID#";
$MESS["CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR5"] = "Произошла ошибка при обровлении ответа на вопрос #ID#";
$MESS["CD_DP_EVENT_ADMIN_FORM_POST_TEMPL_ERR"] = "Почтовый шаблон: " ;


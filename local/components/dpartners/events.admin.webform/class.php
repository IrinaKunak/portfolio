<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Engine\Contract\Controllerable,
    \Bitrix\Main\Engine\ActionFilter;

class EventsAdminDetailForm extends \CBitrixComponent implements Controllerable {
    /* сопоставление верстки и типов в админке */

    protected static $arTypes = [
        "text-input" => "text",
        "select" => "dropdown",
        "email" => "email",
        "checkbox" => "checkbox",
        "radio" => "radio",
        "textarea" => "textarea",
        "file" => "file",
    ];

    /* статусы для клонирования */
    protected static $arStatuses = [
        "new" => 8,
        "wait" => 9,
        "invited" => 10,
        "refused" => 11,
        "come" => 12,
        "not_come" => 13
    ];

    public function executeComponent() {
        $this->getFormQuestions();
        $this->includeComponentTemplate();
    }

    /*

     * ajax метод редактирования формы
     * не использует $this->arParams, $this->arResult
     * @param array $post - массив полей формы
     * @return array - статус результатат и тексты ошибок     
     */

    public function editFormAction($post) {
        AddMessage2Log($post, "edit form");

        $arPost = [];
        $status = "OK";
        foreach ($post as $question) {
            if (strpos($question["name"], '[') !== false) {
                $arTmp = preg_split('/[\[\]]/u', $question["name"], -1, PREG_SPLIT_NO_EMPTY);
            } elseif (strpos($question["name"], '-') !== false) {
                $arTmp = preg_split('/[\-]/u', $question["name"], -1, PREG_SPLIT_NO_EMPTY);
            } else {
                $arTmp[0] = $question["name"];
            }
            if (count($arTmp) == 3) {
                if (!$arPost[$arTmp[0]]) {
                    $arPost[$arTmp[0]] = [];
                    $arPost[$arTmp[0]][$arTmp[1]] = [];
                    $arPost[$arTmp[0]][$arTmp[1]][$arTmp[2]] = [];
                }
                if (!$arPost[$arTmp[0]][$arTmp[1]]) {
                    $arPost[$arTmp[0]][$arTmp[1]] = [];
                    $arPost[$arTmp[0]][$arTmp[1]][$arTmp[2]] = [];
                }
                if (!$arPost[$arTmp[0]][$arTmp[1]][$arTmp[2]]) {
                    $arPost[$arTmp[0]][$arTmp[1]][$arTmp[2]] = [];
                }
                $arPost[$arTmp[0]][$arTmp[1]][$arTmp[2]][] = $question["value"];
            } else {
                $arPost[$arTmp[0]][] = $question["value"];
            }
        }

        AddMessage2Log($arPost);

        if ($arPost["FORM_ID"][0]) {

            foreach ($arPost["QUESTION"] as $id => $arQuestion) {
                $arNewValues = [
                    "FORM_ID" => $arPost["FORM_ID"][0],
                    "TITLE" => $arQuestion["TITLE"][0],
                    "TITLE_TYPE" => "text",
                    "C_SORT" => ($arQuestion["C_SORT"][0] > 0) ? $arQuestion["C_SORT"][0] : 100,
                    "REQUIRED" => ($arQuestion["REQUIRED"][0] == 'on' ) ? "Y" : "N"
                ];


                //если поменялся тип вопроса со списка на строку или текст, удалить все ответы и добавить один новый пустой
                $changeType = false;
                $oldData = CFormField::GetByID($id)->Fetch();
                if (self::$arTypes[$arQuestion["TYPE"][0]] !== $oldData["COMMENTS"]) {
                    $changeType = true;
                    $arNewValues["SID"] = strtoupper(self::$arTypes[$arQuestion["TYPE"][0]] . '_' . random_int(100, 999));
                    $arNewValues["COMMENTS"] = self::$arTypes[$arQuestion["TYPE"][0]];
                    $rsAnswers = CFormAnswer::GetList(
                                    $id,
                                    $by = "s_sort",
                                    $order = "asc",
                                    [],
                                    $is_filtered
                    );
                    while ($arAnswer = $rsAnswers->Fetch()) {
                        $delRes = CFormAnswer::Delete($arAnswer["ID"], $id);
                        if (!$delRes) {
                            $status = "error";
                            $error[] = Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR2", ["#ID#" => $id]);
                        }
                    }
                    if (in_array(self::$arTypes[$arQuestion["TYPE"][0]], ["text", "email", "textarea", "file"])) {
                        $arNewAnswerFields = [
                            "QUESTION_ID" => $id,
                            "MESSAGE" => ' ',
                            "FIELD_TYPE" => self::$arTypes[$arQuestion["TYPE"][0]]
                        ];
                        $resAnswerAdd = CFormAnswer::Set($arNewAnswerFields);
                        if (!$resAnswerAdd) {
                            $status = "error";
                            $error[] = Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR4", ["#ID#" => $questionID]);
                        }
                    }
                }

                if ($arPost["ANSWER"][$id]) {
                    if (!in_array(self::$arTypes[$arQuestion["TYPE"][0]], ["text", "email", "textarea", "file"])) {
                        foreach ($arPost["ANSWER"][$id] as $answerID => $answer) {
                            $arNewAnswerFields = [
                                "QUESTION_ID" => $id,
                                "MESSAGE" => $answer[0],
                                "FIELD_TYPE" => self::$arTypes[$arQuestion["TYPE"][0]]
                            ];
                            if ($changeType) {
                                $resUpdateAnswer = CFormAnswer::Set($arNewAnswerFields);
                            } else {
                                $resUpdateAnswer = CFormAnswer::Set($arNewAnswerFields, $answerID);
                            }
                            if (!$resUpdateAnswer) {
                                $status = "error";
                                $error[] = Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR5", ["#ID#" => $answerID]);
                            }
                        }
                    }
                }


                if ($arPost["answer"][$id]) {
                    if (!in_array(self::$arTypes[$arQuestion["TYPE"][0]], ["text", "email", "textarea", "file"])) {
                        foreach ($arPost["answer"][$id] as $answerID => $answer) {
                            $arNewAnswerFields = [
                                "QUESTION_ID" => $id,
                                "MESSAGE" => ($answer[0] == '') ? "  " : $answer[0],
                                "FIELD_TYPE" => self::$arTypes[$arQuestion["TYPE"][0]]
                            ];
                            $resAnswerAdd = CFormAnswer::Set($arNewAnswerFields);
                            if (!$resAnswerAdd) {
                                $status = "error";
                                $error[] = Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR4", ["#ID#" => $questionID]);
                            }
                        }
                    }
                }
                $res = CFormField::Set($arNewValues, $id);
                if (!$res) {
                    $status = "error";
                    $error[] = Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR", ["#ID#" => $id]);
                }
            }

            foreach ($arPost["QUESTION_NEW"] as $id => $arQuestion) {
                $arNewValues = [
                    "FORM_ID" => $arPost["FORM_ID"][0],
                    "TITLE" => $arQuestion["TITLE"][0],
                    "TITLE_TYPE" => "text",
                    "C_SORT" => ($arQuestion["C_SORT"][0] > 0) ? $arQuestion["C_SORT"][0] : 100,
                    "SID" => strtoupper(self::$arTypes[$arQuestion["TYPE"][0]] . '_' . random_int(100, 999)),
                    "REQUIRED" => ($arQuestion["REQUIRED"][0] == 'on' ) ? "Y" : "N",
                    "COMMENTS" => self::$arTypes[$arQuestion["TYPE"][0]]
                ];
                $questionID = CFormField::Set($arNewValues);

                if (!$questionID) {
                    $status = "error";
                    $error[] = Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR3", ["#ID#" => $id]);
                } else {
                    if ($arPost["answer"][$id]) {
                        foreach ($arPost["answer"][$id] as $answerID => $answer) {
                            $arNewAnswerFields = [
                                "QUESTION_ID" => $questionID,
                                "MESSAGE" => ($answer[0] == '') ? "  " : $answer[0],
                                "FIELD_TYPE" => self::$arTypes[$arQuestion["TYPE"][0]]
                            ];
                            $resAnswerAdd = CFormAnswer::Set($arNewAnswerFields);
                            if (!$resAnswerAdd) {
                                $status = "error";
                                $error[] = Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR4", ["#ID#" => $questionID]);
                            }
                        }
                    }
                }
            }

            //получить статусы формы
            $rsStatuses = CFormStatus::GetList(
                            $arPost["FORM_ID"][0],
                            $by = "s_id",
                            $order = "asc",
                            [],
                            $is_filtered
            );
            while ($arStatus = $rsStatuses->Fetch()) {
                $this->arResult["STATUSES_VALUE"][$arStatus["CSS"]] = [
                    "ID" => $arStatus["ID"],
                    "ACTIVE" => $arStatus["ACTIVE"],
                    "CSS" => $arStatus["CSS"],
                    "TITLE" => $arStatus["TITLE"]
                ];
            }

            foreach ($arPost["STATUS_EVENT"] as $arStatus) {

                if ($this->arResult["STATUSES_VALUE"][$arStatus]) {
                    if ($this->arResult["STATUSES_VALUE"][$arStatus]["ACTIVE"] !== "Y") {
                        CFormStatus::Set(
                                [
                                    "FORM_ID" => $arPost["FORM_ID"][0],
                                    "TITLE" => $this->arResult["STATUSES_VALUE"][$arStatus]["TITLE"],
                                    "ACTIVE" => "Y"
                                ],
                                $this->arResult["STATUSES_VALUE"][$arStatus]["ID"]
                        );
                    }
                } else {
                    //клонировать соответсвующий статус
                    CFormStatus::Copy(self::$arStatuses[$arStatus], "N", $arPost["FORM_ID"][0]);
                }
            }

            //деактивировать не отмеченные в форме статусы
            foreach ($this->arResult["STATUSES_VALUE"] as $code => $arStatus) {
                if ($code == "new")
                    continue;
                if (array_search($code, $arPost["STATUS_EVENT"]) === false) {
                    if ($arStatus["ACTIVE"] == "Y") {
                        CFormStatus::Set(
                                [
                                    "FORM_ID" => $arPost["FORM_ID"][0],
                                    "TITLE" => $arStatus["TITLE"],
                                    "ACTIVE" => "N"
                                ],
                                $arStatus["ID"]
                        );
                    }
                }
            }
        } else {
            //добавление новой формы
            $arNewForm = [
                "BUTTON" => "Отправить",
                "STAT_EVENT1" => "form",
                "arSITE" => ["s1"],
                "USE_CAPTCHA" => "Y"
            ];
            $arNewForm["NAME"] = "Регистрация на событие";
            $arNewForm["SID"] = "REG_FORM_";
            $resEvent = CIBlockElement::GetList([], ["ID" => $arPost["EVENT_ID"][0]], false, false, ["ID", "NAME", "IBLOCK_ID"]);
            if ($arEvent = $resEvent->Fetch()) {
                $arNewForm["NAME"] .= " " . $arEvent["NAME"];
                $arNewForm["SID"] .= $arEvent["ID"];
                $this->arParams["IBLOCK_ID"] = $arEvent["IBLOCK_ID"];
            }
            $arNewForm["STAT_EVENT2"] = $arNewForm["SID"];
            $arNewForm["arMENU"] = [
                "ru" => $arNewForm["NAME"],
                "en" => "Event reg form"
            ];
            $resAddForm = CForm::Set($arNewForm);

            if ($resAddForm) {
                //обновить событие
                 \CIBlockElement::SetPropertyValuesEx((int)$arPost["EVENT_ID"][0], (int)$this->arParams["IBLOCK_ID"], ["REG_FORM_ID" => (int)$resAddForm]);
                global $APPLICATION;
                if ($ex = $APPLICATION->GetException()) {
                    $strError = $ex->GetString();
                }

                //добавить почтовый шаблон
                $rsMess = CEventMessage::GetList($by = "site_id", $order = "desc", ["ID" => 49]);

                if ($arMess = $rsMess->GetNext()) {
                    $arNewPostMessage = [
                        "ACTIVE" => "Y",
                        "LID" => ["s1"],
                        "EMAIL_FROM" => $arMess["EMAIL_FROM"],
                        "EMAIL_TO" => $arMess["EMAIL_TO"],
                        "SUBJECT" => $arMess["SUBJECT"],
                        "MESSAGE" => $arMess["MESSAGE"],
                        "BODY_TYPE" => "html",
                        "EVENT_NAME" => "FORM_FILLING_" . $arNewForm["SID"]
                    ];

                    $idEventMessage = \Bitrix\Main\Mail\Internal\EventMessageTable::add($arNewPostMessage);
                    if ($idEventMessage) {
                        CForm::Set([
                            "NAME" => $arNewForm["NAME"],
                            "SID" => $arNewForm["SID"],
                            "arMAIL_TEMPLATE" => [$idEventMessage]
                                ], $resAddForm);
                    } else {
                        $status = "error";
                        $error[] = Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_POST_TEMPL_ERR") . $em->LAST_ERROR;
                    }

                    //клонировать статусы
                    CFormStatus::Copy(self::$arStatuses["new"], "N", $resAddForm);
                    foreach ($arPost["STATUS_EVENT"] as $arStatus) {
                        CFormStatus::Copy(self::$arStatuses[$arStatus], "N", $resAddForm);
                    }

                    //добавить вопросы и ответы
                    foreach ($arPost["QUESTION_NEW"] as $id => $arQuestion) {
                        $arNewValues = [
                            "FORM_ID" => $resAddForm,
                            "TITLE" => $arQuestion["TITLE"][0],
                            "TITLE_TYPE" => "text",
                            "C_SORT" => ($arQuestion["C_SORT"][0] > 0) ? $arQuestion["C_SORT"][0] : 100,
                            "SID" => strtoupper(self::$arTypes[$arQuestion["TYPE"][0]] . '_' . random_int(100, 999)),
                            "REQUIRED" => ($arQuestion["REQUIRED"][0] == 'on' ) ? "Y" : "N",
                            "COMMENTS" => self::$arTypes[$arQuestion["TYPE"][0]]
                        ];
                        $questionID = CFormField::Set($arNewValues);

                        if (!$questionID) {
                            $status = "error";
                            $error[] = Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR3", ["#ID#" => $id]);
                        } else {
                            if ($arPost["answer"][$id]) {
                                foreach ($arPost["answer"][$id] as $answerID => $answer) {
                                    $arNewAnswerFields = [
                                        "QUESTION_ID" => $questionID,
                                        "MESSAGE" => ($answer[0] == '') ? "  " : $answer[0],
                                        "FIELD_TYPE" => self::$arTypes[$arQuestion["TYPE"][0]]
                                    ];
                                    $resAnswerAdd = CFormAnswer::Set($arNewAnswerFields);
                                    if (!$resAnswerAdd) {
                                        $status = "error";
                                        $error[] = Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_QUESTION_ERROR4", ["#ID#" => $questionID]);
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                global $strError;
                $status = "error";
                $error[] = $strError;
            }
        }

        return [
            "status" => $status,
            "error" => implode('<br/>', $error)
        ];
    }

    /*

     * метод удаления ответа на вопрос без перезагрузки
     * @param array $post - массив вопроса
     * @return string    */

    public function deleteQuestionAction($post) {
        $status = "OK";
        $text = "";
        $res = CFormField::Delete($post["questionID"]);
        if (!$res) {
            global $strError;
            $status = "error";
            $text = json_encode($strError);
        }

        return [
            "status" => $status,
            "error" => $text
        ];
    }

    /*

     * метод удаления ответа на вопрос без перезагрузки
     * @param array $post - массив ИД вопроса и ответа 
     * @return string    */

    public function deleteAnswerAction($post) {
        $status = "OK";

        $res = CFormAnswer::Delete($post["answerID"], $post["questionID"]);
        if (!$res) {
            $status = "error";
        }

        return [
            "status" => $status
        ];
    }

    public function onPrepareComponentParams($arParams) {
        if ($this->checkModules()) {

            if ((int) $arParams["IBLOCK_ID"] < 1) {
                $arParams["IBLOCK_ID"] = 8;
            } else {
                $arParams["IBLOCK_ID"] = (int) $arParams["IBLOCK_ID"];
            }
            $arParams["ELEMENT_ID"] = (int) $arParams["ELEMENT_ID"];

            return ($arParams);
        }
    }

    /*
     * 
     * проверка подключенных модулей
     * 
     * @return boolean - результат проверки модулей
     */

    protected function checkModules() {
        $success = true;

        if (!Loader::includeModule('iblock')) {
            $success = false;
            ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALL'));
        }

        if (!Loader::includeModule('form')) {
            $success = false;
            ShowError(Loc::getMessage('IBLOCK_FORM_NOT_INSTALL'));
        }

        return $success;
    }

    /*
     * 
     * обязательный метод для работы runComponentAjax
     * 
     * @return array - фильтров входящих данных
     */

    public function configureActions() {
        return [
            "updateRegLink" => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                            array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    )
                ]
            ],
            "deleteQuestion" => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                            array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    )
                ]
            ],
            "deleteAnswer" => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                            array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    )
                ]
            ],
            "editForm" => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                            array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    )
                ]
            ],
        ];
    }

    /*
     * 
     * ajax метод обновления ссылки на регистрацию и чекбокса закрытия регистрации
     * @param array $post - массив, содержащий ИД события, действие и значение, которое нужно обновить
     * 
     * @return array - данные, которые в формате json вернутся в ajax
     */

    public function updateRegLinkAction($post) {
        $status = "OK";

        if ($post["action"] == "editUrl") {
            CIBlockElement::SetPropertyValuesEx($post["elementID"], $this->arParams["IBLOCK_ID"], ["REG_LINK" => $post["value"]]);
        }
        if ($post["action"] == "reg_close") {
            $idValue = "";
            if ($post["value"] == "true") {
                $dbProp = CIBlockPropertyEnum::GetList(
                                [
                                    "ID" => "ASC"
                                ],
                                [
                                    "IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "CODE" => "REG_CLOSE"
                                ]
                );

                while ($arProp = $dbProp->GetNext()) {
                    $idValue = $arProp["ID"];
                }
            }
            CIBlockElement::SetPropertyValuesEx($post["elementID"], $this->arParams["IBLOCK_ID"], ["REG_CLOSE" => $idValue]);
        }
        return [
            "status" => $status
        ];
    }

    /*

     * метод получения всех вопросов и вариантов ответов формы
     * @var $this->arResult
     * @var $this->arParams
     *      */

    public function getFormQuestions() {
        $this->arResult["STATUSES"] = [
            "event" => [
                "new" => Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_STATUS1"),
                "wait" => Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_STATUS4"),
                "invited" => Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_STATUS2"),
                "refused" => Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_STATUS3"),
            ],
            "visit" => [
                "come" => Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_STATUS5"),
                "not_come" => Loc::getMessage("CD_DP_EVENT_ADMIN_FORM_STATUS6"),
            ],
        ];

        $this->getEventData();
        if ($this->arResult["EVENT"]["REG_FORM_ID"]) {
            //получить статусы формы

            $rsStatuses = CFormStatus::GetList(
                            $this->arResult["EVENT"]["REG_FORM_ID"],
                            $by = "s_id",
                            $order = "asc",
                            ["ACTIVE" => "Y"],
                            $is_filtered
            );
            while ($arStatus = $rsStatuses->Fetch()) {
                $this->arResult["STATUSES_VALUE"][$arStatus["CSS"]] = [
                    "ID" => $arStatus["ID"],
                    "ACTIVE" => $arStatus["ACTIVE"],
                    "CSS" => $arStatus["CSS"],
                    "TITLE" => $arStatus["TITLE"]
                ];
            }

            $rsQuestions = CFormField::GetList(
                            $this->arResult["EVENT"]["REG_FORM_ID"],
                            "ALL",
                            $by = "s_sort",
                            $order = "asc",
                            ["ACTIVE" => "Y"],
                            $is_filtered
            );
            while ($arQuestion = $rsQuestions->Fetch()) {
                //  \Bitrix\Main\Diag\Debug::dump($arQuestion);
                unset($arNewQuestion);
                $arNewQuestion = [
                    "ID" => $arQuestion["ID"],
                    "TITLE" => $arQuestion["TITLE"],
                    "C_SORT" => $arQuestion["C_SORT"],
                    "REQUIRED" => $arQuestion["REQUIRED"]
                ];
                $rsAnswers = CFormAnswer::GetList(
                                $arQuestion["ID"],
                                $by = "s_sort",
                                $order = "asc",
                                [
                                    "ACTIVE" => "Y"
                                ],
                                $is_filtered
                );
                while ($arAnswer = $rsAnswers->Fetch()) {
                    if (!$arNewQuestion["TYPE"])
                        $arNewQuestion["TYPE"] = $arAnswer["FIELD_TYPE"];
                    $arNewQuestion["ANSWERS"][] = [
                        "ID" => $arAnswer["ID"],
                        "MESSAGE" => $arAnswer["MESSAGE"],
                        "SORT" => $arAnswer["C_SORT"]
                    ];
                }
                $this->arResult["QUESTIONS"][] = $arNewQuestion;
            }
        }
    }

    /*

     * получить данные события - ИД формы, ссылку на регистрацию, признак завершения регистрации
     * @var  arParams, arResult
     *      */

    public function getEventData() {
        $dbRes = CIBlockElement::GetList(
                        [],
                        [
                            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                            "ID" => $this->arParams["ELEMENT_ID"]
                        ],
                        false,
                        false,
                        ["ID", "IBLOCK_ID", "PROPERTY_REG_CLOSE", "PROPERTY_REG_LINK", "PROPERTY_REG_FORM_ID"]
        );
        if ($arRes = $dbRes->Fetch()) {
            $this->arResult["EVENT"] = [
                "ID" => $arRes["ID"],
                "REG_CLOSE" => $arRes["PROPERTY_REG_CLOSE_VALUE"],
                "REG_LINK" => $arRes["PROPERTY_REG_LINK_VALUE"],
                "REG_FORM_ID" => $arRes["PROPERTY_REG_FORM_ID_VALUE"]
            ];
        }
    }

}

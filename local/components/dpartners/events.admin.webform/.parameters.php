<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;

$arComponentParameters = [
    "PARAMETERS" => [
        "IBLOCK_ID" => [
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("COMP_DP_EVENTS_ADMIN_IBLOCK_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "8",
        ],
        "ELEMENT_ID" => [
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("COMP_DP_EVENTS_ADMIN_ELEMENT_ID"),
            "TYPE" => "STRING",
            "DEFAULT" => "1",
        ],
        "CACHE_TIME" => ["DEFAULT" => 36000000],
    ]
];

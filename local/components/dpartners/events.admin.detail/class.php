<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader,
    \Bitrix\Main\Engine\Contract\Controllerable,
    \Bitrix\Main\Engine\ActionFilter,
    \Bitrix\Main\Application,
    Bitrix\Main\Type\DateTime;

/* ToDo: кеширование */

class EventsAdminDetail extends \CBitrixComponent implements Controllerable {

    public static function transliterate($st) {

        $st = mb_strtolower($st);
        $st = preg_replace('/[^\p{L}0-9\!]/iu', '-', $st);

        $st = str_replace(
                [
                    'а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'й',
                    'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у',
                    'ф', 'ы', 'э', 'ё', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ь',
                    'ю', 'я', ' '
                ],
                [
                    'a', 'b', 'v', 'g', 'd', 'e', 'g', 'z', 'i', 'y',
                    'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u',
                    'f', 'i', 'e', "yo", "h", "ts", "ch", "sh",
                    "shch", '_', '_', "yu", "ya", '_'
                ],
                $st);

        $st = trim($st, "-");
        return $st;
    }

    public function executeComponent() {
        /* отправлена форма */
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        if ($this->arParams["ELEMENT_ID"] == 0) {
            $this->getNewEventProps();
        } else {
            if ($request->isPost()) {
                $post = $request->getPostList()->toArray();
                $files = $request->getFileList()->toArray();
                $this->editElement($post, $files);
            }

            $this->getEvent();
        }
        $this->includeComponentTemplate();
    }

    /*
     * 
     * добавление нового события
     * 
     * @var arResult
     */

    public function newEventAction() {
        $status = "ok";
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        $post = $request->getPostList()->toArray();
        $files = $request->getFileList()->toArray();

        $el = new CIBlockElement;
        //заполнить ERRORS
        $this->arResult["ERROR_FIELDS"] = [];

        if (strlen(trim($post["NAME"])) > 0) {
            $arNewFields = [
                "NAME" => $post["NAME"],
                "CODE" => self::transliterate($post["NAME"])
            ];
        } else {
            $this->arResult["ERROR_FIELDS"][] = "NAME";
        }

        //картинки
        if ($files["PREVIEW_PICTURE"]["tmp_name"]) {
            $arNewFields["PREVIEW_PICTURE"] = $files["PREVIEW_PICTURE"];
        }
        if ($files["DETAIL_PICTURE"]["tmp_name"]) {
            $arNewFields["DETAIL_PICTURE"] = $files["DETAIL_PICTURE"];
        }

        //презентация - файл
        if ($files["PRESENTATION"]["tmp_name"]) {
            $arNewProps["PDF_DESC"] = $files["PRESENTATION"];
        }

        foreach ($post as $name => $prop) {
            if ($this->arResult["PROPERTIES"][$name]["IS_REQUIRED"] == "Y" && !$prop) {
                $this->arResult["ERROR_FIELDS"][] = $name;
            }
            switch ($name) {
                case "NAME":
                    continue 2;
                case "DATE_ACTION": case "DATE_END":
                    $objDateTime = new DateTime($prop);
                    $arNewProps[$name] = $objDateTime->toString(\Bitrix\Main\Context::getCurrent()->getCulture());
                    break;
                case "ACTIVE":
                    $arNewFields["ACTIVE"] = ($prop == "on") ? "Y" : "N";
                    break;
                case "DETAIL_TEXT":
                    $arNewFields["DETAIL_TEXT"] = $prop;
                    break;
                case "SHOW_IN_SLIDER":
                    $rsProp = CIBlockPropertyEnum::GetList([], ["IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "CODE" => "SHOW_IN_SLIDER"]);
                    $yes = $rsProp->Fetch();
                    $arNewProps["SHOW_IN_SLIDER"] = ($prop == "on") ? $yes["ID"] : "";
                    break;
                default:
                    $arNewProps[$name] = $prop;
            }
        }

        if (!$post["ACTIVE"]) {
            $arNewFields["ACTIVE"] = "N";
        }

        if (!$post["SHOW_IN_SLIDER"]) {
            $arNewProps["SHOW_IN_SLIDER"] = "";
        }
        $arNewFields["IBLOCK_ID"] = $this->arParams["IBLOCK_ID"];
        $arNewFields["PROPERTY_VALUES"] = $arNewProps;

        $res = $el->Add($arNewFields);
        if ($res) {
            $this->arResult["NEW_ELEMENT_ID"] = $res;
            //запись события добавлена, можно добавлять программы
            //программы
            $el = new \CIBlockElement;
            $arNewProps["PROGRAM"] = [];
            foreach ($post["PROGRAM_NAME"] as $id => $pogr) {
                $arFields = [
                    "NAME" => $pogr,
                    "DETAIL_TEXT" => $post["PROGRAM_DETAIL_TEXT"][$id],
                    "IBLOCK_ID" => $this->arParams["PROGRAM_IBLOCK_ID"]
                ];

                $arProps = [
                    "START_TIME" => $post["PROGRAM_START_TIME"][$id],
                    "SPEAKER" => $post["PROGRAM_SPEAKERS"][$id]
                ];
                //презентация - файл
                if ($files["PROGRAM_PRES"]["tmp_name"][$id]) {
                    $arProps["PRESENTATION"] = [
                        'name' => $files["PROGRAM_PRES"]["name"][$id],
                        'tmp_name' => $files["PROGRAM_PRES"]["tmp_name"][$id],
                        'type' => $files["PROGRAM_PRES"]["type"][$id],
                        'error' => $files["PROGRAM_PRES"]["error"][$id],
                        'size' => $files["PROGRAM_PRES"]["size"][$id],
                    ];
                }

                $arFields["PROPERTY_VALUES"] = $arProps;
                $res = $el->Add($arFields);
                if ($res) {
                    $arNewProps["PROGRAM"][] = $res;
                } else {
                    $status = "error";
                    $this->arResult["ERRORS"]["PRORGAM"] = Loc::getMessage("CP_DP_EVENTS_ADMIN_ELEMENT_ADD_ERROR_PROGRAMM") . '<br>' . $el->LAST_ERROR;
                }
            }
            foreach ($post["PROGRAM_NAME_NEW"] as $id => $pogr) {
                $arFields = [
                    "NAME" => $pogr,
                    "DETAIL_TEXT" => $post["PROGRAM_DETAIL_TEXT_NEW"][$id],
                    "IBLOCK_ID" => $this->arParams["PROGRAM_IBLOCK_ID"]
                ];

                if (!$pogr) {
                    $this->arResult["ERROR_FIELDS"][$id] = "PROGRAM_NAME_NEW";
                }

                $arProps = [
                    "START_TIME" => $post["PROGRAM_START_TIME_NEW"][$id],
                    "SPEAKER" => $post["PROGRAM_SPEAKERS_NEW"]["NEW_".$id]
                ];
                //презентация - файл
                if ($files["PROGRAM_PRES"]["tmp_name"][$id]) {
                    $arProps["PRESENTATION"] = [
                        'name' => $files["PROGRAM_PRES"]["name"][$id],
                        'tmp_name' => $files["PROGRAM_PRES"]["tmp_name"][$id],
                        'type' => $files["PROGRAM_PRES"]["type"][$id],
                        'error' => $files["PROGRAM_PRES"]["error"][$id],
                        'size' => $files["PROGRAM_PRES"]["size"][$id],
                    ];
                }
                
                $arFields["PROPERTY_VALUES"] = $arProps;
                $res = $el->Add($arFields);
                if ($res) {
                    $arNewProps["PROGRAM"][] = $res;
                } else {
                    $status = "error";
                    $this->arResult["ERRORS"]["PROGRAM"] = Loc::getMessage("CP_DP_EVENTS_ADMIN_ELEMENT_ADD_ERROR_PROGRAMM") . '<br>' . $el->LAST_ERROR;
                }
            }
            if ($arNewProps["PROGRAM"] && !empty($arNewProps["PROGRAM"])) {
                CIBlockElement::SetPropertyValuesEx($this->arResult["NEW_ELEMENT_ID"], $this->arResult["IBLOCK_ID"], $arNewProps);
            }
        } else {
            $this->arResult["ERRORS"]["ELEMENT"] = Loc::getMessage("CP_DP_EVENTS_ADMIN_ELEMENT_ADD_ERROR_PROGRAMM") . '<br>' . $el->LAST_ERROR;
            $status = "error";
        }
        // AddMessage2log([$status, $this->arResult["NEW_ELEMENT_ID"], $this->arResult["ERRORS"], $this->arResult["ERROR_FIELDS"]]);
        return [
            "status" => $status,
            "id" => $this->arResult["NEW_ELEMENT_ID"],
            "error" => json_encode($this->arResult["ERRORS"], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_LINE_TERMINATORS),
            "fields" => json_encode($this->arResult["ERROR_FIELDS"])
        ];
    }

    /*
     * 
     * редактирование события
     * 
     * @var arResult
     */

    public function editElement($post, $files) {
        //заполнить ERRORS
        $arNewFields = [
            "NAME" => $post["NAME"]
        ];

        //картинки
        if ($files["PREVIEW_PICTURE"]["tmp_name"]) {
            $arNewFields["PREVIEW_PICTURE"] = $files["PREVIEW_PICTURE"];
        }
        if ($files["DETAIL_PICTURE"]["tmp_name"]) {
            $arNewFields["DETAIL_PICTURE"] = $files["DETAIL_PICTURE"];
        }


        //программы
        $el = new \CIBlockElement;
        $arNewProps["PROGRAM"] = [];
        foreach ($post["PROGRAM_NAME"] as $id => $pogr) {
            $arFields = [
                "NAME" => $pogr,
                "DETAIL_TEXT" => $post["PROGRAM_DETAIL_TEXT"][$id]
            ];

            $arProps = [
                "START_TIME" => $post["PROGRAM_START_TIME"][$id],
                "SPEAKER" => $post["PROGRAM_SPEAKERS"][$id]
            ];
            //презентация - файл
            if ($files["PROGRAM_PRES"]["tmp_name"][$id]) {
                $arProps["PRESENTATION"] = [
                    'name' => $files["PROGRAM_PRES"]["name"][$id],
                    'tmp_name' => $files["PROGRAM_PRES"]["tmp_name"][$id],
                    'type' => $files["PROGRAM_PRES"]["type"][$id],
                    'error' => $files["PROGRAM_PRES"]["error"][$id],
                    'size' => $files["PROGRAM_PRES"]["size"][$id],
                ];
            }

            $arFields["PROPERTY_VALUES"] = $arProps;
            if ($id) {
                $res = $el->Update($id, $arFields);
            }

            $arNewProps["PROGRAM"][] = $id;
        }

        foreach ($post["PROGRAM_NAME_NEW"] as $id => $pogr) {
            $arFields = [
                "NAME" => $pogr,
                "DETAIL_TEXT" => $post["PROGRAM_DETAIL_TEXT_NEW"][$id],
                "IBLOCK_ID" => $this->arParams["PROGRAM_IBLOCK_ID"]
            ];

            $arProps = [
                "START_TIME" => $post["PROGRAM_START_TIME_NEW"][$id],
                "SPEAKER" => $post["PROGRAM_SPEAKERS_NEW"]["NEW_".$id]
            ];
            //презентация - файл
            if ($files["PROGRAM_PRES"]["tmp_name"][$id]) {
                $arProps["PRESENTATION"] = [
                    'name' => $files["PROGRAM_PRES"]["name"][$id],
                    'tmp_name' => $files["PROGRAM_PRES"]["tmp_name"][$id],
                    'type' => $files["PROGRAM_PRES"]["type"][$id],
                    'error' => $files["PROGRAM_PRES"]["error"][$id],
                    'size' => $files["PROGRAM_PRES"]["size"][$id],
                ];
            }

            $arFields["PROPERTY_VALUES"] = $arProps;
            $res = $el->Add($arFields);

            $arNewProps["PROGRAM"][] = $res;
        }

        //презентация - файл
        if ($files["PRESENTATION"]["tmp_name"]) {
            $arNewProps["PDF_DESC"] = $files["PRESENTATION"];
        }

        foreach ($post as $name => $prop) {
            switch ($name) {
                case "NAME":
                    continue 2;
                case "DATE_ACTION": case "DATE_END":
                    $objDateTime = new DateTime($prop);
                    $arNewProps[$name] = $objDateTime->toString(\Bitrix\Main\Context::getCurrent()->getCulture());
                    break;
                case "ACTIVE":
                    $arNewFields["ACTIVE"] = ($prop == "on") ? "Y" : "N";
                    break;
                case "DETAIL_TEXT":
                    $arNewFields["DETAIL_TEXT"] = $prop;
                    break;
                case "SHOW_IN_SLIDER":
                    $rsProp = CIBlockPropertyEnum::GetList([], ["IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "CODE" => "SHOW_IN_SLIDER"]);
                    $yes = $rsProp->Fetch();
                    $arNewProps["SHOW_IN_SLIDER"] = ($prop == "on") ? $yes["ID"] : "";
                    break;
                default:
                    $arNewProps[$name] = $prop;
            }
        }

        if (!$post["ACTIVE"]) {
            $arNewFields["ACTIVE"] = "N";
        }

        if (!$post["SHOW_IN_SLIDER"]) {
            $arNewProps["SHOW_IN_SLIDER"] = "";
        }

        $arNewFields["PROPERTY_VALUES"] = $arNewProps;
        $res = $el->Update($this->arParams["ELEMENT_ID"], $arNewFields);
    }

    public function onPrepareComponentParams($arParams) {
        if ($this->checkModules()) {

            if ((int) $arParams["IBLOCK_ID"] < 1) {
                $arParams["IBLOCK_ID"] = 8;
            } else {
                $arParams["IBLOCK_ID"] = (int) $arParams["IBLOCK_ID"];
            }
            $arParams["ELEMENT_ID"] = (int) $arParams["ELEMENT_ID"];
            $arParams["SPEAKER_IBLOCK_ID"] = \Portal\Tools::getIDIblockByCode("speakers");
            $arParams["PROGRAM_IBLOCK_ID"] = \Portal\Tools::getIDIblockByCode("program_ru", "references");

            return ($arParams);
        }
    }

    /*
     * 
     * проверка подключенных модулей
     * 
     * @return boolean - результат проверки модулей
     */

    protected function checkModules() {
        $success = true;

        if (!Loader::includeModule('iblock')) {
            $success = false;
            ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALL'));
        }

        if (!Loader::includeModule('form')) {
            $success = false;
            ShowError(Loc::getMessage('IBLOCK_FORM_NOT_INSTALL'));
        }

        return $success;
    }

    /*

     * получить поля и свойства для нового события
     * @staticvar arResult
     * @staticvar arParams
     *  */

    protected function getNewEventProps() {
        $this->arResult["ELEMENT"] = [
            "ID" => "",
            "TITLE" => "",
            "ACTIVE" => "",
            "DETAIL_TEXT" => ""
        ];
        $dbRes = CIBlockProperty::GetList(
                        [
                            "sort" => "asc", "name" => "asc"
                        ],
                        [
                            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"]
                        ]
        );
        while ($arRes = $dbRes->Fetch()) {
            if ($arRes["CODE"] == "REG_LINK" || $arRes["CODE"] == "REG_CLOSE")
                continue;
            $arNewProps = $arRes;
            if ($arRes["CODE"] == "PROGRAM" || $arRes["CODE"] == "MODERATOR") {
                $dbSpeakerProp = CIBlockProperty::GetList([], ["IBLOCK_ID" => $this->arParams["PROGRAM_IBLOCK_ID"], "CODE" => "SPEAKER"]);
                if ($arSpeakerProp = $dbSpeakerProp->Fetch()) {
                    $arNewProps["ALL_SPEAKERS"] = $this->GetAllValues($arSpeakerProp);
                }
            }
            if ($arRes["CODE"] == "TAGS") {
                $dbSpeakerProp = CIBlockProperty::GetList([], ["CODE" => "TAGS"]);
                if ($arSpeakerProp = $dbSpeakerProp->Fetch()) {
                    $arNewProps["ALL_VALUES"] = $this->GetAllValues($arSpeakerProp);
                }
            }
            if ($arRes["CODE"] == "EVENT_PLACE") {
                $dbSpeakerProp = CIBlockProperty::GetList([], ["CODE" => "EVENT_PLACE"]);
                if ($arSpeakerProp = $dbSpeakerProp->Fetch()) {
                    $arNewProps["ALL_VALUES"] = $this->GetAllValues($arSpeakerProp);
                }
            }
            $this->arResult["PROPERTIES"][$arRes["CODE"]] = $arNewProps;
        }
    }

    /*

     * получить поля и свойства события
     * @staticvar arResult
     * @staticvar arParams
     *  */

    protected function getEvent() {
        $arSelect = [
            "ID",
            "NAME",
            "IBLOCK_ID",
            "IBLOCK_SECTION_ID",
            "DETAIL_TEXT",
            "DETAIL_TEXT_TYPE",
            "PREVIEW_TEXT",
            "PREVIEW_TEXT_TYPE",
            "PREVIEW_PICTURE",
            "DETAIL_PICTURE",
            "ACTIVE",
        ];


        /* получить список всех свойств инфоблока */
        $resIBProps = \CIBlockProperty::GetList(["sort" => "asc"], ["IBLOCK_ID" => $this->arParams["IBLOCK_ID"]]);
        while ($arResIBProp = $resIBProps->GetNext()) {
            $arSelect[] = "PROPERTY_" . $arResIBProp["CODE"];
        }
        $dbRes = \CIBlockElement::GetList(
                        [],
                        [
                            "IBLOCK_ID" => $this->arParams["IBLOCK_ID"],
                            "ID" => $this->arParams["ELEMENT_ID"]
                        ],
                        false,
                        false,
                        $arSelect
        );
        if ($obRes = $dbRes->GetNextElement()) {
            $arFileds = $obRes->getFields();
            $arProps = $obRes->getProperties();

            $this->arResult["ELEMENT"] = [
                "ID" => $arFileds["ID"],
                "TITLE" => $arFileds["NAME"],
                "ACTIVE" => $arFileds["ACTIVE"],
                "DETAIL_TEXT" => $arFileds["DETAIL_TEXT"]
            ];

            if ($arFileds["PREVIEW_PICTURE"]) {
                $arFilePreview = \CFile::ResizeImageGet($arFileds["PREVIEW_PICTURE"], ["width" => 300, "height" => 300], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
                $this->arResult["ELEMENT"]["PREVIEW_PICTURE"] = $arFilePreview;
            }

            if ($arFileds["DETAIL_PICTURE"]) {
                $arFileDetail = \CFile::ResizeImageGet($arFileds["DETAIL_PICTURE"], ["width" => 300, "height" => 300], BX_RESIZE_IMAGE_PROPORTIONAL_ALT, true);
                $this->arResult["ELEMENT"]["DETAIL_PICTURE"] = $arFileDetail;
            }

            foreach ($arProps as $prop) {
                if ($prop["CODE"] == "REG_LINK" || $prop["CODE"] == "REG_CLOSE")
                    continue;

                $newProp = $this->GetDisplayValueCustom($prop);
                /* дополучить спикеров */
                if ($prop["CODE"] == "PROGRAM") {

                    $resPropProgramm = \CIBlockElement::GetList(
                                    [
                                        "SORT" => "ASC"
                                    ],
                                    [
                                        "ID" => $prop["VALUE"],
                                        "IBLOCK_ID" => $prop["LINK_IBLOCK_ID"]
                                    ],
                                    false,
                                    false,
                                    [
                                        "ID", "PROPERTY_SPEAKER.NAME", "PROPERTY_SPEAKER.ID"
                                    ]
                    );
                    while ($arResPropProgramm = $resPropProgramm->Fetch()) {
                        if ($arResPropProgramm["PROPERTY_SPEAKER_ID"]) {
                            $newProp["SPEAKERS"][$arResPropProgramm["ID"]][$arResPropProgramm["PROPERTY_SPEAKER_ID"]] = [
                                "ID" => $arResPropProgramm["PROPERTY_SPEAKER_ID"],
                                "NAME" => $arResPropProgramm["PROPERTY_SPEAKER_NAME"],
                            ];
                        }
                    }
                    $dbSpeakerProp = CIBlockProperty::GetList([], ["IBLOCK_ID" => $prop["LINK_IBLOCK_ID"], "CODE" => "SPEAKER"]);
                    if ($arSpeakerProp = $dbSpeakerProp->Fetch()) {
                        $newProp["ALL_SPEAKERS"] = $this->GetAllValues($arSpeakerProp);
                    }
                }
                $this->arResult["PROPERTIES"][$prop["CODE"]] = $newProp;
            }
        } else {
            \Bitrix\Iblock\Component\Tools::process404(
                    "404!!"
                    , true
                    , true
                    , true
                    , ""
            );
            return;
        }
    }

    /*

     * получить все варианты значения свойства типа привязка к элементу инфоблока
     * @param array $arProperty - свойство инфоблока типа привязка к элементу
     * @return array
     *  */

    public function GetAllValues($arProperty) {
        $result = [];
        $res = \CIBlockElement::GetList(
                        [
                            "SORT" => "ASC"
                        ],
                        [
                            "IBLOCK_ID" => $arProperty["LINK_IBLOCK_ID"],
                            "ACTIVE" => "Y"
                        ],
                        false,
                        false,
                        [
                            "ID", "NAME"
                        ]
        );
        while ($arRes = $res->Fetch()) {
            $result[] = $arRes;
        }
        return $result;
    }

    /*

     * метод взят из ядра CIBlockFormatProperties::GetDisplayValue, выпилена статистика и Б24    */

    public function GetDisplayValueCustom($arProperty) {

        if ($arProperty["PROPERTY_TYPE"] == "E" && $arProperty["CODE"] !== "PROGRAM") {
            $arProperty["ALL_VALUES"] = $this->GetAllValues($arProperty);
        }

        /** @var array $arUserTypeFormat */
        $arUserTypeFormat = false;
        if (isset($arProperty["USER_TYPE"]) && !empty($arProperty["USER_TYPE"])) {
            $arUserType = \CIBlockProperty::GetUserType($arProperty["USER_TYPE"]);
            if (isset($arUserType["GetPublicViewHTML"]))
                $arUserTypeFormat = $arUserType["GetPublicViewHTML"];
        }

        static $CACHE = array("E" => array(), "G" => array());
        if ($arUserTypeFormat) {
            if ($arProperty["MULTIPLE"] == "N" || !is_array($arProperty["~VALUE"]))
                $arValues = array($arProperty["~VALUE"]);
            else
                $arValues = $arProperty["~VALUE"];
        } else {
            if (is_array($arProperty["VALUE"]))
                $arValues = $arProperty["VALUE"];
            else
                $arValues = array($arProperty["VALUE"]);
        }
        $arDisplayValue = array();
        $arFiles = array();

        foreach ($arValues as $val) {

            if ($arUserTypeFormat) {
                $arDisplayValue[] = call_user_func_array($arUserTypeFormat,
                        array(
                            $arProperty,
                            array("VALUE" => $val),
                            array(),
                ));
            } elseif ($arProperty["PROPERTY_TYPE"] == "E") {
                if (intval($val) > 0) {
                    if (!isset($CACHE["E"][$val])) {
                        //USED TO GET "LINKED" ELEMENTS
                        $arLinkFilter = [
                            "ID" => $val,
                            "ACTIVE" => "Y",
                            "ACTIVE_DATE" => "Y",
                            "CHECK_PERMISSIONS" => "Y",
                        ];

                        if ($arProperty["CODE"] == "PROGRAM") {
                            $arSelectFromPropE = ["ID", "IBLOCK_ID", "NAME", "DETAIL_TEXT", "PROPERTY_PRESENTATION", "PROPERTY_START_TIME"];
                        } else {
                            $arSelectFromPropE = ["ID", "IBLOCK_ID", "NAME", "SORT"];
                        }

                        $rsLink = \CIBlockElement::GetList(
                                        ["sort" => "asc"],
                                        $arLinkFilter,
                                        false,
                                        false,
                                        $arSelectFromPropE
                        );
                    }
                    $CACHE["E"][$val] = $rsLink->GetNext();

                    if (is_array($CACHE["E"][$val])) {
                        if ($arProperty["CODE"] == "PROGRAM") {
                            if ($CACHE["E"][$val]["PROPERTY_PRESENTATION_VALUE"]) {
                                $arPres = \CFile::GetFileArray($CACHE["E"][$val]["PROPERTY_PRESENTATION_VALUE"]);
                            }
                            $arDisplayValue[] = [
                                "ID" => $CACHE["E"][$val]["ID"],
                                "NAME" => $CACHE["E"][$val]["NAME"],
                                "DETAIL_TEXT" => $CACHE["E"][$val]["DETAIL_TEXT"],
                                "START_TIME" => $CACHE["E"][$val]["PROPERTY_START_TIME_VALUE"],
                                "SORT" => $CACHE["E"][$val]["SORT"],
                                "PRESENTATION" => $arPres
                            ];
                        } else {
                            $arDisplayValue[] = $CACHE["E"][$val]["NAME"];
                        }
                    }
                }
            } elseif ($arProperty["PROPERTY_TYPE"] == "G") {
                if (intval($val) > 0) {
                    if (!isset($CACHE["G"][$val])) {
                        //USED TO GET SECTIONS NAMES
                        $arSectionFilter = array(
                            "ID" => $val,
                        );
                        $rsSection = \CIBlockSection::GetList(
                                        array(),
                                        $arSectionFilter,
                                        false,
                                        array("ID", "IBLOCK_ID", "NAME", "SECTION_PAGE_URL", "PICTURE", "DETAIL_PICTURE", "SORT")
                        );
                        $CACHE["G"][$val] = $rsSection->GetNext();
                    }
                    if (is_array($CACHE["G"][$val])) {
                        $arDisplayValue[] = $CACHE["G"][$val]["NAME"];
                    }
                }
            } elseif ($arProperty["PROPERTY_TYPE"] == "L") {
                $arDisplayValue[] = $val;
            } elseif ($arProperty["PROPERTY_TYPE"] == "F") {
                if ($arFile = \CFile::GetFileArray($val)) {
                    $arFiles[] = $arFile;
                    $arDisplayValue[] = '<a class="presentation__link" href="' . htmlspecialcharsbx($arFile["SRC"]) . '">' . $arFile['FILE_NAME'] . '</a>';
                }
            } else {
                $trimmed = trim($val);
                if (strpos($trimmed, "http") === 0) {
                    $arDisplayValue[] = '<a href="' . htmlspecialcharsbx($trimmed) . '">' . $trimmed . '</a>';
                } elseif (strpos($trimmed, "www") === 0) {

                    $arDisplayValue[] = '<a href="' . htmlspecialcharsbx("http://" . $trimmed) . '">' . $trimmed . '</a>';
                } else
                    $arDisplayValue[] = $val;
            }
        }
        $displayCount = count($arDisplayValue);
        if ($displayCount == 1) {
            $arProperty["DISPLAY_VALUE"] = $arDisplayValue[0];
        } elseif ($displayCount > 1) {

            if ($arProperty["CODE"] == "PROGRAM") {
                uasort($arDisplayValue, 'sortBySort');
            }
            $arProperty["DISPLAY_VALUE"] = $arDisplayValue;
        } else {
            $arProperty["DISPLAY_VALUE"] = false;
        }

        if ($arProperty["PROPERTY_TYPE"] == "F") {
            $fileCount = count($arFiles);
            if ($fileCount == 1)
                $arProperty["FILE_VALUE"] = $arFiles[0];
            elseif ($fileCount > 1)
                $arProperty["FILE_VALUE"] = $arFiles;
            else
                $arProperty["FILE_VALUE"] = false;
        }

        return $arProperty;
    }

    /*
     * 
     * обязательный метод для работы runComponentAjax
     * 
     * @return array - фильтров входящих данных
     */

    public function configureActions() {
        return [
            "updateRegLink" => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                            array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    )
                ]
            ],
            "newEvent" => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                            array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    )
                ]
            ],
            "addSpeaker" => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                            array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    )
                ]
            ],
            "delProgram" => [
                'prefilters' => [
                    new ActionFilter\HttpMethod(
                            array(ActionFilter\HttpMethod::METHOD_GET, ActionFilter\HttpMethod::METHOD_POST)
                    )
                ]
            ],
        ];
    }

    /*
     * 
     * ajax метод удаления программы события
     * @param array $post - массив, содержащий ИД программы
     * 
     * @return array - данные, которые в формате json вернутся в ajax
     */

    public static function delProgramAction($post) {
        $status = "OK";
        $text = "";

        $el = new CIBlockElement;

        $el->Delete($post["progID"]);

        return [
            "status" => $status,
            "error" => $text
        ];
    }

    /*
     * 
     * ajax метод обновления ссылки на регистрацию и чекбокса закрытия регистрации
     * @param array $post - массив, содержащий ИД события, действие и значение, которое нужно обновить
     * 
     * @return array - данные, которые в формате json вернутся в ajax
     */

    public static function updateRegLinkAction($post) {
        $status = "OK";
        if ($post["action"] == "editUrl") {
            CIBlockElement::SetPropertyValuesEx($post["eventID"], $this->arParams["IBLOCK_ID"], ["REG_LINK" => $post["value"]]);
        }
        if ($post["action"] == "reg_close") {
            $idValue = "";
            if ($post["value"] == "true") {
                $dbProp = CIBlockPropertyEnum::GetList(
                                [
                                    "ID" => "ASC"
                                ],
                                [
                                    "IBLOCK_ID" => $this->arParams["IBLOCK_ID"], "CODE" => "REG_CLOSE"
                                ]
                );

                while ($arProp = $dbProp->GetNext()) {
                    $idValue = $arProp["ID"];
                }
            }
            CIBlockElement::SetPropertyValuesEx($post["eventID"], $this->arParams["IBLOCK_ID"], ["REG_CLOSE" => $idValue]);
        }
        return [
            "status" => $status
        ];
    }

    /*
     * 
     * ajax метод добавления спикера
     * @param array $post - массив, содержащий поля для ИБ спикера
     * 
     * @return array - данные, которые в формате json вернутся в ajax
     */

    public function addSpeakerAction() {
        $status = "OK";
        $error = [];

        $arNewFields = [];
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();

        $files = $request->getFileList()->toArray();
        $post = $request->getPostList()->toArray();

        foreach ($post as $name => $p) {
            switch ($name) {
                case "SPEAKER_NAME":
                    $clear = htmlspecialchars(trim($p));
                    if (strlen($clear) < 1) {
                        $error["SPEAKER_NAME"] = "SPEAKER_NAME";
                        $status = "error";
                    } else {
                        $arNewFields["NAME"] = $clear;
                    }
                    break;
                case "SPEAKER_STATUS":
                    $clear = htmlspecialchars(trim($p));
                    if (strlen($clear) > 1) {
                        $arNewFields["PROPS"]["FUNCTION"] = $clear;
                    }
                    break;
                case "SPEAKER_ABOUT":
                    if (strlen($p) > 1) {
                        $arNewFields["DETAIL_TEXT"] = $p;
                    }
                    break;
            }
        }


        if ($files["SPEAKER_PHOTO"]["tmp_name"]) {
            $arNewFields["PREVIEW_PICTURE"] = $files["SPEAKER_PHOTO"];
        }

        if (!empty($arNewFields)) {
            $arNewFields["ACTIVE"] = "Y";
            $arNewFields["IBLOCK_ID"] = $this->arParams["SPEAKER_IBLOCK_ID"];

            $el = new CIBlockElement;
            $res = $el->Add($arNewFields);
            if (!$res) {
                $status = "error";
                $error["ERROR_TEXT"] = Loc::getMessage("CP_DP_EVENTS_ADMIN_ELEMENT_ADD_ERROR");
            } else {
                $status = $res;
            }
        }
        return [
            "status" => $status,
            "name" => $arNewFields["NAME"],
            "error" => json_encode($error)
        ];
    }

}

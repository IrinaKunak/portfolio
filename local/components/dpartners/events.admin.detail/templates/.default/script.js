$(function () {
    $(document).on("click", ".eventSpeakerPopup", function () {
        $("input[name='prorgamID']").val($(this).attr("data-program-id"));
    });
    $("#eventSpeakerPopup").on('hide.bs.modal', function () {
        console.log("hide");
        $("input[name='prorgamID']").val();
    });

    $(document).on("click", ".js-remove-report", function () {
        var progID = $(this).attr('data-del-id');
        BX.ajax.runComponentAction('dpartners:events.admin.detail', 'delProgram', {
            mode: 'class',
            data: {
                post: {
                    progID: progID
                }
            }
        }).catch(function (response) {
            console.log(response);
        })
    });
})

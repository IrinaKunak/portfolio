
(function ($) {
    $.fn.serializeFiles = function () {
        var form = $(this),
                formData = new FormData();
        formParams = form.serializeArray();

        $.each(form.find('input[type="file"]'), function (i, tag) {
            $.each($(tag)[0].files, function (i, file) {
                formData.append(tag.name, file);
            });
        });

        $.each(formParams, function (i, val) {
            formData.append(val.name, val.value);
        });

        return formData;
    };
})(jQuery);
$(function () {
    $(document).on("click", ".eventSpeakerPopup", function () {
        $("input[name='prorgamID']").val($(this).attr("data-program-id"));
    });
    $("#eventSpeakerPopup").on('hide.bs.modal', function () {
        $("input[name='prorgamID']").val();
    });

    $(document).on("submit", "#addElementForm", function (e) {
        e.preventDefault();
        var post = $("#addElementForm").serializeFiles();

        BX.ajax.runComponentAction('dpartners:events.admin.detail', 'newEvent', {
            mode: 'class',
            data: post
        }).then(function (response) {
            console.log(response);
            $(".error.red").detach();
            if (response.data.status == "error") {
                var errorText = JSON.parse(response.data.error);
                for (el in errorText) {
                    $("#addElementForm").prepend('<div class="error red">' + errorText[el] + '</div>');
                }

                if (response.data.id) {
                    setSessionParam("BX_EVENT_ADMIN_ACTIVE_TAB", 'base');
                    location.href = '/events-admin/' + response.data.id + '/';
                } else {
                    var scrollToTop = $("#addElementForm").offset().top;
                    $(document).scrollTop(scrollToTop);
                }

                //подсвечивать ошибочные поля
//                var errorText = JSON.parse(response.data.fields);
//                for (el in errorText) {
//                    $("#addElementForm").prepend('<div class=error red>' +errorText[el] + '</div>');
//                }

            } else {
                setSessionParam("BX_EVENT_ADMIN_ACTIVE_TAB", 'base');
                location.href = '/events-admin/' + response.data.id + '/';
            }

        }).catch(function (response) {
            console.log(response);
        })
    })

    $(document).on("submit", "#addSpeaker", function (e) {
        e.preventDefault();
        //       var post = $("#addSpeaker").serializeArray();
        var post = $("#addSpeaker").serializeFiles();

        BX.ajax.runComponentAction('dpartners:events.admin.detail', 'addSpeaker', {
            mode: 'class',
            data: post
        })
                .then(function (response) {
                    console.log(response);
                    if (response.data.status !== "error" && response.data.name) {
                        $('.js-select-speakers').each(function () {
                            var name = $(this).attr("name"),
                                    programID = $("input[name='prorgamID']").val();
                            if (name.indexOf(programID) < 0) {
                                $(this).append('<option value="' + response.data.status + '">' + response.data.name + '</option>');
                            } else {
                                $(this).append('<option value="' + response.data.status + '" selected>' + response.data.name + '</option>');
                            }
                        });
                        // $("#eventSpeakerPopup").hide();
                    } else {
                        var error = JSON.parse(response.data.error);
                        if (error.SPEAKER_NAME) {
                            $("input[name='SPEAKER_NAME']").addClass("red-error");
                        }
                        if (error.ERROR_TEXT) {
                            $(".error.red").detach();
                            $("#addSpeaker").prepend('<div class="error red">' + error.ERROR_TEXT + '</div>');
                        }
                    }

                })
                .catch((response) => {
                    console.log(response);

                });
    })
})
<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use Bitrix\Main\Localization\Loc;
?>
<?
//Bitrix\Main\Diag\Debug::dump($arResult["PROPERTIES"]);
?>
<form action="" class="form" method="POST" enctype="multipart/form-data" id="addElementForm">
    <input type="hidden" name="ELEMENT_ID" value="">
    <h2 class="page-title"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_TITLE_H2") ?></h2>
    <div class="event__wrap">
        <div class="event__row">
            <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_TITLE_EVENT"); ?></div>
            <div class="event__control">
                <div class="form-group">
                    <input class="form-control" name="NAME" type="text" placeholder="" value="<?= $arResult["ELEMENT"]["TITLE"] ?>">
                </div>
            </div>
        </div>
        <!--все простые текстовые свойства!-->
        <?
        foreach ($arResult["PROPERTIES"] as $prop) {
            if ($prop["PROPERTY_TYPE"] == "S") {
                if ($prop["USER_TYPE"] !== "HTML") {
                    if ($prop["CODE"] == "DATE_ACTION" || $prop["CODE"] == "DATE_END") {
                        ?>
                        <div class="event__row">
                            <div class="event__note"><?= $prop["NAME"] ?></div>
                            <div class="event__control">
                                <div class="form-group">
                                    <input class="form-control js-datetimepicker"
                                           type="text"
                                           name="<?= $prop["CODE"] ?>"
                                           placeholder=""
                                           value="<?= $prop["VALUE"] ?>"
                                           <? if ($prop["IS_REQUIRED"] == "Y") { ?>required<? } ?>>                                    
                                </div>
                            </div>
                        </div>
                    <? } else { ?>

                        <? if ($prop["CODE"] == "REG_FORM_ID") { ?>
                            <input type="hidden" name="<?= $prop["CODE"] ?>" value="<?= $prop["VALUE"] ?>">
                        <? } else { ?>
                            <div class="event__row">
                                <div class="event__note"><?= $prop["NAME"] ?></div>
                                <div class="event__control">
                                    <div class="form-group">
                                        <input class="form-control"
                                               type="text"
                                               name="<?= $prop["CODE"] ?>"
                                               placeholder=""
                                               value="<?= $prop["VALUE"] ?>"
                                               <? if ($prop["IS_REQUIRED"] == "Y") { ?>required<? } ?> >
                                    </div>
                                </div>
                            </div>
                            <?
                        }
                    }
                }
            }
            ?>

        <? } ?>
        <!--показывать в слайдере на странице списка!-->
        <div class="event__row">
            <div class="form-group form-group--checkbox">
                <label class="form-checkbox">
                    <input name="<?= $arResult["PROPERTIES"]["SHOW_IN_SLIDER"]["CODE"] ?>" type="checkbox" 
                           <? if ($arResult["PROPERTIES"]["SHOW_IN_SLIDER"]["IS_REQUIRED"] == 'Y') { ?>required<? } ?>
                           <? if ($arResult["PROPERTIES"]["SHOW_IN_SLIDER"]["DISPLAY_VALUE"] == 'Да') { ?>checked<? } ?>>
                    <span class="form-checkbox__text"><?= $arResult["PROPERTIES"]["SHOW_IN_SLIDER"]["NAME"] ?></span>
                </label>
            </div>
        </div>
        <!--активность!-->
        <div class="event__row">
            <div class="form-group form-group--checkbox">
                <label class="form-checkbox">
                    <input name="ACTIVE" type="checkbox" <? if ($arResult["ELEMENT"]["ACTIVE"] == "Y") { ?>checked<? } ?>>
                    <span class="form-checkbox__text"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_ACTIVE_EVENT"); ?></span>
                </label>
            </div>
        </div>
        <!--Теги!-->
        <div class="event__row">
            <div class="event__note"><?= $arResult["PROPERTIES"]["TAGS"]["NAME"] ?></div>
            <div class="event__control">
                <div class="form-group">
                    <select class="js-select" name="TAGS[]" multiple="multiple" style="width: 100%">
                        <? foreach ($arResult["PROPERTIES"]["TAGS"]["ALL_VALUES"] as $val) { ?>
                            <option value="<?= $val["ID"] ?>" <? if (in_array($val["ID"], $arResult["PROPERTIES"]["TAGS"]["VALUE"])) { ?>selected<? } ?>><?= $val["NAME"] ?></option>
                        <? } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <!--Блок о мероприятии!-->
    <h2 class="page-title"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_ABOUT") ?></h2>
    <div class="event__wrap">
        <div class="event__row">
            <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_ABOUT2") ?></div>
            <div class="event__control">
                <div class="form-group">
                    <textarea class="form-control form-control--textarea" name="DETAIL_TEXT" placeholder=""><?= html_entity_decode($arResult["ELEMENT"]["DETAIL_TEXT"]) ?></textarea>
                </div>
            </div>
        </div>
        <div class="event__row">
            <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_ABOUT22") ?></div>
            <div class="event__control">
                <div class="form-group">
                    <textarea class="form-control form-control--textarea" name="<?= $arResult["PROPERTIES"]["ABOUT"]["CODE"] ?>" placeholder="" <? if ($arResult["PROPERTIES"]["ABOUT"] == "Y") { ?>required<? } ?>><?= html_entity_decode($arResult["PROPERTIES"]["ABOUT"]["VALUE"]["TEXT"]) ?></textarea>
                </div>
            </div>
        </div>
    </div>
    <!--Картинки!-->
    <h2 class="page-title"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PICT") ?></h2>
    <div class="event__wrap">
        <div class="event__imgs">
            <div class="event__img-wrap">
                <span class="event__img-note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PICT1") ?></span>
                <? if ($arResult["ELEMENT"]["PREVIEW_PICTURE"]) { ?>
                    <div class="event__img" style="background-image: url('<?= $arResult["ELEMENT"]["PREVIEW_PICTURE"]["src"] ?>')" id="imagePreview"></div>
                <? } else { ?>
                    <div class="event__img isEmpty" style="background-image: url('')" id="imagePreview"></div>
                <? } ?>

                <div data-api="api/" class="event__img-control" id="upload-form">
                    <label class="event__change-img btn btn--inverted">
                        <input type="file" id="imageUpload" class="js-img-change" name="PREVIEW_PICTURE" accept=".png, .jpg, .jpeg">
                        <?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PICT_E") ?>
                    </label>
                    <span class="event__change-error js-upload-error">
                        <?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PICT_S") ?>
                    </span>
                </div>
            </div>
            <div class="event__img-wrap">
                <span class="event__img-note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PICT2") ?></span>
                <? if ($arResult["ELEMENT"]["DETAIL_PICTURE"]) { ?>
                    <div class="event__img" style="background-image: url('<?= $arResult["ELEMENT"]["DETAIL_PICTURE"]["src"] ?>')" id="imagePreview2"></div>
                <? } else { ?>
                    <div class="event__img isEmpty" style="background-image: url('')" id="imagePreview2"></div>
                <? } ?>

                <div data-api="api/" class="event__img-control" id="upload-form2">
                    <label class="event__change-img btn btn--inverted">
                        <input type="file" id="imageUpload2" class="js-img-change" name="DETAIL_PICTURE" accept=".png, .jpg, .jpeg">
                        <?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PICT_E") ?>
                    </label>
                    <span class="event__change-error js-upload-error">
                        <?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PICT_S") ?>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <!--программа мероприятия!-->
    <h2 class="page-title"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PROG") ?></h2>

    <div class="event__wrap">
        <div class="event__row">
            <div class="form-file-wrap">
                <label class="form-file">
                    <input class="form-file__control js-file-upload" type="file" id="file-upload" name="PRESENTATION">
                    <span class="form-file__load"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PRES") ?> <svg class="svg"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/sprite.svg#link"/></svg></span>
                </label>
                <span class="form-file__name js-remove-file"><span class="text"></span><svg class="svg"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/sprite.svg#close"/></svg></span>
            </div>
        </div>
        <div class="event__report">
            <div class="event__top">
                <h3 class="event__title"> <?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_DOC") ?> 1</h3>
                <button type="button" class="event__delete js-remove-report">
                    <svg class="svg"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/sprite.svg#close"/></svg>
                </button>
            </div>         
                <div class="event__row">
                        <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_DOC2") ?></div>                    
                    <div class="event__control">
                        <div class="form-group">
                            <input class="form-control" name="PROGRAM_NAME[1]" type="text" placeholder="" value="">
                        </div>
                    </div>
                </div>
                <div class="event__row">
                    <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_PROGR_TIME") ?></div>
                    <div class="event__control">
                        <div class="form-group">
                            <input class="form-control" name="PROGRAM_START_TIME[1]" type="text" placeholder="" value="">
                        </div>
                    </div>
                </div>
                <div class="event__row">
                    <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_PROGR_TEXT") ?></div>
                    <div class="event__control">
                        <div class="form-group">
                            <textarea class="form-control form-control--textarea" name="PROGRAM_DETAIL_TEXT[1]" placeholder="" ></textarea>
                        </div>
                    </div>
                </div>

                <!--спикеры!-->
                <div class="event__row">
                    <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_SPEAKER") ?></div>
                    <div class="event__control event__control--full">
                        <div class="form-group">
                            <select class="js-select js-select-speakers" name="PROGRAM_SPEAKERS[1]" multiple="multiple" style="width: 100%">
                                <? foreach ($arResult["PROPERTIES"]["PROGRAM"]["ALL_SPEAKERS"] as $val) { ?>
                                    <option value="<?= $val["ID"] ?>"><?= $val["NAME"] ?></option>
                                <? } ?>
                            </select>
                        </div>
                        <div class="event__btn">
                            <a href="#eventSpeakerPopup" data-toggle="modal" data-target="#eventSpeakerPopup" data-program-id="<?= $prog["ID"] ?>" class="btn btn--default eventSpeakerPopup js-add-speaker" type="submit"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_MODAL_TITLE2") ?></a>
                        </div>
                    </div>
                </div>
                <div class="event__row">
                    <div class="form-file-wrap">
                        <label class="form-file">
                            <input class="form-file__control js-file-upload" name="PROGRAM_PRES[1]" type="file" id="report-file-upload">
                            <span class="form-file__load"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PRES") ?> <svg class="svg"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/sprite.svg#link"/></svg></span>
                        </label>
                        <span class="form-file__name js-remove-file"><span class="text"></span><svg class="svg"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/sprite.svg#close"/></svg></span>
                    </div>
                </div>

        </div>
     	<script id="report-template" type="x-tmpl-mustache">
            <div class="event__report">
                <div class="event__top">
                    <h3 class="event__title"> <?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_DOC") ?> {{index}}</h3>
                    <button type="button" class="event__delete js-remove-report">
                        <svg class="svg"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/sprite.svg#close"/></svg>
                    </button>
                </div>
                <div class="event__row">
                    <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_DOC2") ?></div>
                    <div class="event__control">
                        <div class="form-group">
                            <input class="form-control" name="PROGRAM_NAME_NEW[{{index}}]" type="text" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="event__row">
                    <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_PROGR_TIME") ?></div>
                    <div class="event__control">
                        <div class="form-group">
                            <input class="form-control" type="text" name="PROGRAM_START_TIME_NEW[{{index}}]" placeholder="">
                        </div>
                    </div>
                </div>
                <div class="event__row">
                    <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_PROGR_TEXT") ?></div>
                    <div class="event__control">
                        <div class="form-group">
                            <textarea class="form-control form-control--textarea" name="PROGRAM_DETAIL_TEXT_NEW[{{index}}]" placeholder=""></textarea>
                        </div>
                    </div>
                </div>
                <div class="event__row">
                    <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_SPEAKER") ?></div>
                    <div class="event__control event__control--full">
                        <div class="form-group">
                            <select class="js-select js-select-speakers" name="PROGRAM_SPEAKERS_NEW[NEW_{{index}}][]" multiple="multiple" style="width: 100%">
                                {{#speakers}}
                                    	<option value="{{value}}">{{title}}</option>
				{{/speakers}}
                            </select>
                        </div>
                        <div class="event__btn">
                            <a href="#eventSpeakerPopup" data-toggle="modal" data-target="#eventSpeakerPopup" class="btn btn--default eventSpeakerPopup js-add-speaker" data-program-id="new_{{index}}" type="submit"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_MODAL_TITLE2") ?></a>
                        </div>
                    </div>
                </div>
                <div class="event__row">
                    <div class="form-file-wrap">
                        <label class="form-file">
                            <input class="form-file__control js-file-upload" name="PROGRAM_PRES[{{index}}]" type="file" id="report-file-{{index}}">
                            <span class="form-file__load"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PRES2") ?> <svg class="svg"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/sprite.svg#link"/></svg></span>
                        </label>
                        <span class="form-file__name js-remove-file"><span class="text"></span><svg class="svg"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/sprite.svg#close"/></svg></span>
                    </div>
                </div>
            </div>
        </script>   
        <div class="reports" id="reports-target" data-speakers-api="/ajax/getAllSpeakers.php" data-index="1"></div>        
        <div class="event__add-btn">
            <button class="btn btn--inverted js-add-report"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_PROG_ADD") ?></button>
        </div>
    </div>

    <!--Модератор!-->
    <h2 class="page-title"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_MODER") ?></h2>
    <div class="event__wrap">
        <div class="event__row">
            <div class="event__note"><?= $arResult["PROPERTIES"]["MODERATOR"]["NAME"] ?></div>
            <div class="event__control">
                <div class="form-group">
                    <select class="js-select js-select-single" name="MODERATOR" multiple="multiple" style="width: 100%">
                        <? foreach ($arResult["PROPERTIES"]["MODERATOR"]["ALL_SPEAKERS"] as $val) { ?>
                            <option value="<?= $val["ID"] ?>" <? if ($arResult["PROPERTIES"]["MODERATOR"]["VALUE"] == $val["ID"]) { ?>selected<? } ?>><?= $val["NAME"] ?></option>
                        <? } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    
    <!--Место проведения мероприятия!-->
    <h2 class="page-title"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_LOC") ?></h2>
    <div class="event__wrap">
        <div class="event__row">
            <div class="event__note"><?= $arResult["PROPERTIES"]["EVENT_PLACE"]["NAME"] ?></div>
            <div class="event__control">
                <div class="form-group">
                    <select class="js-select js-select-single" name="EVENT_PLACE" multiple="multiple" style="width: 100%">
                        <? foreach ($arResult["PROPERTIES"]["EVENT_PLACE"]["ALL_VALUES"] as $val) { ?>
                            <option value="<?= $val["ID"] ?>" <? if ($arResult["PROPERTIES"]["EVENT_PLACE"]["VALUE"] == $val["ID"]) { ?>selected<? } ?>><?= $val["NAME"] ?></option>
                        <? } ?>
                    </select>
                </div>
            </div>
        </div>
    </div>
    <div class="event__btn">
        <button class="btn btn--default" type="submit"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_SAVE") ?></button>
    </div>
</form>
<!--модалка!-->
<div class="modal modal-info fade" id="eventSpeakerPopup" tabindex="-1" role="dialog" aria-labelledby="detailLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="modal-info__close" data-dismiss="modal" aria-label="Close">
                    <svg class="svg"><use xlink:href="<?= SITE_TEMPLATE_PATH ?>/assets/sprite.svg#close"/></svg>
                </button>
                <div class="modal-info__content">
                    <h3><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_MODAL_TITLE") ?></h3>
                    <form action="" method="POST" class="form" id="addSpeaker" enctype="multipart/form-data">
                        <input type="hidden" name="SPEAKER_IBLOCK_ID" value="<?= $arParams["SPEAKER_IBLOCK_ID"] ?>">
                        <input type="hidden" name="prorgamID" id="prorgamID" value="">
                        <div class="event__wrap">
                            <div class="event__row">
                                <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_MODAL_NAME") ?></div>
                                <div class="event__control">
                                    <div class="form-group">
                                        <input class="form-control" name="SPEAKER_NAME" type="text" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="event__row">
                                <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_MODAL_STATUS") ?></div>
                                <div class="event__control">
                                    <div class="form-group">
                                        <input class="form-control" name="SPEAKER_STATUS" type="text" placeholder="">
                                    </div>
                                </div>
                            </div>
                            <div class="event__row">
                                <div class="event__note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_MODAL_ABOUT") ?></div>
                                <div class="event__control">
                                    <div class="form-group">
                                        <textarea class="form-control form-control--textarea" name="SPEAKER_ABOUT" placeholder=""></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="event__row">
                                <div class="event__img-wrap">
                                    <span class="event__img-note"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PICT1") ?></span>
                                    <div class="event__img isEmpty" style="background-image: url('')" id="imagePreview3"></div>

                                    <div data-api="api/" class="event__img-control" id="upload-form3">
                                        <label class="event__change-img btn btn--inverted">
                                            <input type="file" id="imageUpload3" class="js-img-change" name="SPEAKER_PHOTO" accept=".png, .jpg, .jpeg">
                                            <?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PICT_E") ?>
                                        </label>
                                        <span class="event__change-error js-upload-error">
                                            <?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_PICT_S") ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-info__btns">
                            <button class="btn btn--default"  type="submit"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_MODAL_TITLE") ?></button>
                            <button class="btn btn--inverted" data-dismiss="modal" aria-label="Close"><?= Loc::getMessage("CP_DP_EVENTS_DETAIL_EVENT_CANCEL") ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader;

class CDPartnersEventsAdmin extends CBitrixComponent {

    public function executeComponent() {
        $componentPage = $this->getSEFResult();
        $this->includeComponentTemplate($componentPage);
    }

    public function onPrepareComponentParams($arParams) {
        if ($this->checkModules()) {
            if ((int) $arParams["IBLOCK_ID"] < 1) {
                $arParams["IBLOCK_ID"] = 8;
            } else {
                $arParams["IBLOCK_ID"] = (int) $arParams["IBLOCK_ID"];
            }
            if (strlen($this->arParams["IBLOCK_TYPE"]) < 1) {
                $arParams["IBLOCK_TYPE"] = "content";
            }
            return ($arParams);
        }
    }

    /*
     * 
     * проверка подключенных модулей
     * 
     * @return boolean - результат проверки модулей
     */

    protected function checkModules() {
        $success = true;

        if (!Loader::includeModule('iblock')) {
            $success = false;
            ShowError(Loc::getMessage('IBLOCK_MODULE_NOT_INSTALL'));
        }

        if (!Loader::includeModule('form')) {
            $success = false;
            ShowError(Loc::getMessage('IBLOCK_FORM_NOT_INSTALL'));
        }

        return $success;
    }

    /*

     * Режим ЧПУ - парсиг урла    
     * @return array SEF variables 
     */

    protected function getSEFResult() {
        $arDefaultUrlTemplates404 = [
            'list' => 'index.php',
            'detail' => '#ELEMENT_ID#.php',
            'add' => 'ADD_ELEMENT/'
        ];

        $arDefaultVariableAliases404 = [];
        $arDefaultVariableAliases = [];
        $arComponentVariables = ['ELEMENT_ID'];
        $SEF_FOLDER = '';
        $arUrlTemplates = [];

        if ($this->arParams['SEF_MODE'] == 'Y') {

            $arVariables = [];

            $arUrlTemplates = CComponentEngine::MakeComponentUrlTemplates(
                            $arDefaultUrlTemplates404,
                            $this->arParams['SEF_URL_TEMPLATES']
            );

            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases(
                            $arDefaultVariableAliases404,
                            $this->arParams['VARIABLE_ALIASES']
            );

            $componentPage = CComponentEngine::ParseComponentPath(
                            $this->arParams['SEF_FOLDER'],
                            $arUrlTemplates,
                            $arVariables
            );

            if (strlen($componentPage) <= 0) {
                $componentPage = 'list';
            }

            CComponentEngine::InitComponentVariables(
                    $componentPage,
                    $arComponentVariables,
                    $arVariableAliases,
                    $arVariables);

            $SEF_FOLDER = $this->arParams['SEF_FOLDER'];
        } else {
            $arVariables = [];

            $arVariableAliases = CComponentEngine::MakeComponentVariableAliases(
                            $arDefaultVariableAliases,
                            $this->arParams['VARIABLE_ALIASES']
            );

            CComponentEngine::InitComponentVariables(
                    false,
                    $arComponentVariables,
                    $arVariableAliases,
                    $arVariables
            );

            $componentPage = '';

            if (intval($arVariables['ELEMENT_ID']) > 0) {
                $componentPage = 'detail';
            } else {
                $componentPage = 'list';
            }
        }

        $this->arResult = [
            'FOLDER' => $SEF_FOLDER,
            'URL_TEMPLATES' => $arUrlTemplates,
            'VARIABLES' => $arVariables,
            'ALIASES' => $arVariableAliases,
        ];
        return $componentPage;
    }

}

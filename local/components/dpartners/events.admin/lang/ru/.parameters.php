<?php
$MESS["CP_DP_COMP_EVENTS_ADMIN_IBLOCK_TYPES"] = "Тип инфоблока";
$MESS["CP_DP_EVENTS_ADMIN_IBLOCK_ID"] = "ИД инфоблока";
$MESS["CP_DP_EVENTS_ADMIN_ELEMENT_ID"] = "Идентификатор элемента";
$MESS["CP_DP_EVENTS_ADMIN_PAGE_LIST"] = "Список";
$MESS["CP_DP_EVENTS_ADMIN_PAGE_DETAIL"] = "Детальная";
$MESS["CP_DP_EVENTS_ADMIN_PAGE_COUNT"] = "Количество событий в блоке";
$MESS["CP_DP_EVENTS_ADMIN_EVENTS_INVITE_URL"] = "URL страницы приглашенных";


<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$arComponentDescription = array(
    "NAME" => GetMessage("COMP_DP_EVENTS_ADMIN"),
    "DESCRIPTION" => GetMessage("COMP_DP_EVENTS_ADMIN_DESCR"),
    "COMPLEX" => "Y",
    "PATH" => array(
        "ID" => "dpartners",
    ),
);
?>
<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? //\Bitrix\Main\Diag\Debug::dump($arResult);      ?>
<?
$currentTab = $_SESSION["BX_EVENT_ADMIN_ACTIVE_TAB"];
//\Bitrix\Main\Diag\Debug::dump($_SESSION["BX_EVENT_ADMIN_ACTIVE_TAB"])
?>
<a href="/events-admin/" class="link-back">
    <svg class="svg"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/sprite.svg#back-arrow"></use></svg>
    вернуться назад
</a>

<ul class="nav nav-tabs">
    <li class="nav-item <? if (!$currentTab || $currentTab == "base") { ?> active <? } ?>" data-tab-name="base">
        <a class="nav-link" data-toggle="tab" href="#base">Базовая информация</a>
    </li>
    <li class="nav-item <? if ($currentTab == "register") { ?> active <? } ?>" data-tab-name="register">
        <a class="nav-link" data-toggle="tab" href="#register">Регистрация на событие</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade <? if (!$currentTab || $currentTab == "base") { ?> show active <? } ?>" id="base">
        <?
        $APPLICATION->IncludeComponent(
                "dpartners:events.admin.detail",
                "",
                Array(
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
                )
        );
        ?>
    </div>
    <div class="tab-pane fade <? if ($currentTab == "register") { ?> show active <? } ?>" id="register"> 
        <?
        $APPLICATION->IncludeComponent(
                "dpartners:events.admin.webform",
                "",
                Array(
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "ELEMENT_ID" => $arResult["VARIABLES"]["ELEMENT_ID"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"]
                )
        );
        ?>
    </div>
</div>
<script>
    $(function () {
        $(document).on("click", "li.nav-item", function () {
            var tab = $(this).attr("data-tab-name");
            setSessionParam("BX_EVENT_ADMIN_ACTIVE_TAB", tab);
        })
    })
</script>

<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?

$APPLICATION->IncludeComponent(
        "dpartners:events.admin.list",
        "",
        Array(
            "CACHE_TIME" => $arParams["CACHE_TIME"],
            "CACHE_TYPE" => $arParams["CACHE_TYPE"],
            "IBLOCK_ID" => $arParams["IBLOCK_ID"],
            "EVENTS_PAGE_COUNT" => $arParams["EVENTS_PAGE_COUNT"],
            "EVENTS_SHOW_ALL" => "N",
            "SEF_MODE" => $arParams["SEF_MODE"],
            "SEF_FOLDER" => $arParams["SEF_FOLDER"],
            "EVENTS_INVITE_URL" => $arParams["EVENTS_INVITE_URL"],
            "SEF_URL_TEMPLATES" => $arParams["SEF_URL_TEMPLATES"]
        ),
        $component
);
?>


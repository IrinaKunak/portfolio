<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<a href="/events-admin/" class="link-back">
    <svg class="svg"><use xlink:href="<?=SITE_TEMPLATE_PATH?>/assets/sprite.svg#back-arrow"></use></svg>
    вернуться назад
</a>

<ul class="nav nav-tabs">
    <li class="nav-item active">
        <a class="nav-link" data-toggle="tab" href="#base">Базовая информация</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#register">Регистрация на событие</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane fade show active" id="base">
        <?
        $APPLICATION->IncludeComponent(
                "dpartners:events.admin.detail",
                "new",
                Array(
                    "CACHE_TIME" => $arParams["CACHE_TIME"],
                    "CACHE_TYPE" => $arParams["CACHE_TYPE"],
                    "IBLOCK_ID" => $arParams["IBLOCK_ID"],
                    "ELEMENT_ID" => 0,
                )
        );
        ?>
    </div>
    <div class="tab-pane fade" id="register"> 
        <?$APPLICATION->IncludeComponent(
	"dpartners:events.admin.webform",
	"",
	Array(
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"ELEMENT_ID" => "4123",
		"IBLOCK_ID" => "8"
	)
);?>
    </div>
</div>


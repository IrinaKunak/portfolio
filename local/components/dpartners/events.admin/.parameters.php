<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

use \Bitrix\Main\Localization\Loc,
    \Bitrix\Main\Loader;

if (!Loader::includeModule("iblock"))
    return;

$arIBlockTypes = CIBlockParameters::GetIBlockTypes();

$resIBlocks = CIBlock::GetList(
                [
                    "SORT" => "ASC"
                ],
                [
                    "TYPE" => $arCurrentValues["IBLOCK_TYPE"],
                    "ACTIVE" => "Y"
                ]
);
while ($arIBlocksRes = $resIBlocks->Fetch()) {
    $arIBlocks[$arIBlocksRes["ID"]] = "[" . $arIBlocksRes["ID"] . "] " . $arIBlocksRes["NAME"];
}

$arComponentParameters = [
    "PARAMETERS" => [
        "IBLOCK_TYPE" => [
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("CP_DP_COMP_EVENTS_ADMIN_IBLOCK_TYPES"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlockTypes,
            "DEFAULT" => "content",
            "REFRESH" => "Y"
        ],
        "IBLOCK_ID" => [
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_IBLOCK_ID"),
            "TYPE" => "LIST",
            "VALUES" => $arIBlocks,
            "DEFAULT" => 8
        ],
        "EVENTS_PAGE_COUNT" => [
          "PARENT" => "BASE",
            "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_PAGE_COUNT"),
            "TYPE" => "STRING",
            "DEFAULT" => 3
        ],
        "VARIABLE_ALIASES" => [
            "ELEMENT_ID" => [
                "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_ELEMENT_ID")
            ],
        ],
        "EVENTS_INVITE_URL" => [
          "PARENT" => "BASE",
            "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_EVENTS_INVITE_URL"),
            "TYPE" => "STRING",
            "DEFAULT" => '/events-invite/'
        ],        
        "SEF_MODE" => [
            "list" => [
                "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_PAGE_LIST"),
                "DEFAULT" => "",
                "VARIABLES" => [],
            ],
            "detail" => [
                "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_PAGE_DETAIL"),
                "DEFAULT" => "#ELEMENT_ID#/",
                "VARIABLES" => ['ELEMENT_ID'],
            ],
            "add" => [
                "NAME" => Loc::getMessage("CP_DP_EVENTS_ADMIN_PAGE_ADD"),
                "DEFAULT" => "ADD_ELEMENT/",
                "VARIABLES" => [],
            ],
        ],
        "CACHE_TIME" => ["DEFAULT" => 36000000]
    ]
];
